Integrantes: ALEJO FERNÁNDEZ DE LA TORRE ; CAROLINA MONTE


El ejecutable .jar requiere Java 8 para correr. De otra forma el juego puede inciarse desde el compilador,
pero es necesario importar las librerías de JavaFX. Esto se puede hacer en Eclipse instalando el plugin
e(fx)clipse y luego importando las librerías necesarias en el proyecto. Debido a limitaciones de JUnit al
hacer coverage del código, las líneas que arrojan excepciones no son cubiertas, aunque existan pruebas que
efectivamente prueban los casos excepcionales. Junto con el proyecto se suben dos archivos ejecutables;
MonsterBattle.jar incluye el juego con la interfaz básica y MonsterBattleRemastered.jar incluye el juego
con la interfaz gráfica mejorada.
<br>
<br>
<br>
## Decisiones de diseño

En principio queríamos implementar polimorfismo de la siguiente manera: 
El método de ataque recibía los tipos de monstruo por separado y con un entero elegía qué tipo era.
Resultó que esta implementación no evoluciona adecuadamente, por ejemplo si se pudiese tener hasta tres tipos,
y decidimos utilizar Visitors, que es la solución que encontramos para no usar enteros como tipos
de monstruos sino recibir el tipo en sí y utilizar de manera más eficiente las propiedades del polimorfismo.
<br>
<br>
<br>
## Clases

**MonsterType** *(Interfaz)*  
Métodos abstractos que serán definidos luego por clases que implementan la interfaz
<br>
<br>
**AirType - EarthType - FireType - WaterType** >>> *implementan la interfaz MonsterType*  
El método calculateDamage llama al método defend. Recibe uno de los tipos del monstruo
enemigo, chequea si el ataque es especial o no.
Crea el visitor de su tipo.
<br>
<br>
**MonsterTypeVisitor** *(Interfaz)*  
Métodos abstractos que serán definidos luego por clases que implementan la interfaz
<br>
<br>
**AirTypeVisitor - EarthTypeVisitor - FireTypeVisitor - WaterTypeVisitor** >>> *implementan la interfaz MonsterTypeVisitor*  
Método defend definido en cada uno para cada tipo, contiene todas las posibilidades de ataques y calcula el daño recibido.
<br>
<br>
**Player**  
Setea nombre del jugador y guarda un Monstruo para su uso en el juego.
<br>
<br>
**Monster**  
Crea un monstruo con 100 puntos de vida, 4 ataques especiales y hasta dos tipos. Estos pueden ser fuego, aire,
agua o tierra, e interactúan de acuerdo a las siguientes relaciones:
* Fuego inflige más daño contra aire y resiste tierra
* Agua inflige más daño contra fuego y resiste fuego
* Aire inflige más daño contra tierra y resiste aire
* Tierra inflige más daño contra agua y resiste agua

Nota: Si el mismo tipo es elegido dos veces, el monstruo verá su resistencia y debilidad duplicadas.
También es posible elegir un solo tipo. No afecta al ataque.
<br>
<br>
**MonsterBattle**  
Guarda los dos jugadores que participan del juego y dispone la interfaz para el usuario/aplicación:
Setea los los monstruos de cada jugador.
Opera los ataques de cada jugador.
Chequea si terminó la batalla; devuelve el nombre del ganador.
<br>
<br>
**NoSpecialAttacksLeftException**  
Excepción arrojada cuando el monstruo no tiene más ataques especiales y se intenta atacar con uno.
<br>
<br>
**GameOverException**  
Excepción arrojada cuando terminó el juego y se intenta seguir jugando.
<br>
<br>
<br>
## Conclusiones

Decidimos implementar Visitors porque era la solución más cercana a lo que nos habíamos imaginado 
al principio, si bien no es lo más sencillo de hacer, pues podríamos haber implementado un enumerador,
una solución que también evolucionaría apropiadamente, si se agregasen más tipos por ejemplo.