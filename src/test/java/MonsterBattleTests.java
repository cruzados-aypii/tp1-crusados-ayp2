import org.junit.Test;

import logicFunctionality.*;

import static org.junit.Assert.*;

import org.junit.After;

public class MonsterBattleTests {

	@Test
	public void createMonsterBattleAndChangeTurn() throws NoSpecialAttacksLeftException, GameOverException {
		MonsterBattle game = new MonsterBattle("Acos", "Peros");

		game.setPlayer1Monster(new FireType(), null);
		game.setPlayer2Monster(new WaterType(), new EarthType());

		assertTrue(game.getPlayer1Turn());
		assertEquals(100, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertEquals(100, game.getPlayer1().getPlayerMonster().getHealthPoints());

		game.playerAttacks(1);

		assertFalse(game.getPlayer1Turn());
		assertEquals(92, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertEquals(100, game.getPlayer1().getPlayerMonster().getHealthPoints());

		game.playerAttacks(1);

		assertTrue(game.getPlayer1Turn());
		assertEquals(92, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertEquals(88, game.getPlayer1().getPlayerMonster().getHealthPoints());
	}

	@After
	public void resetPlayerCounter1() {
		Player.resetNumberOfPlayers();
	}

	@Test(expected = GameOverException.class)
	public void player1Wins() throws NoSpecialAttacksLeftException, GameOverException {
		MonsterBattle game = new MonsterBattle("Peros", "Acos");

		game.setPlayer1Monster(new AirType(), new FireType());
		game.setPlayer2Monster(new EarthType(), new EarthType());

		assertEquals(100, game.getPlayer1().getPlayerMonster().getHealthPoints());
		assertEquals(100, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertEquals("", game.getWinner());

		game.playerAttacks(2);
		game.playerAttacks(2);
		assertEquals(79, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertEquals(88, game.getPlayer1().getPlayerMonster().getHealthPoints());
		assertFalse(game.isOver());

		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(1);

		assertEquals(52, game.getPlayer1().getPlayerMonster().getHealthPoints());
		assertEquals(2, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertFalse(game.isOver());

		game.playerAttacks(1);
		game.playerAttacks(1);

		assertEquals(0, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertTrue(game.isOver());
		assertEquals("Peros", game.getWinner());

		game.playerAttacks(1);// throws the expected exception, line required to pass the test
	}

	@After
	public void resetPlayerCounter2() {
		Player.resetNumberOfPlayers();
	}

	@Test(expected = GameOverException.class)
	public void player2Wins() throws NoSpecialAttacksLeftException, GameOverException {
		MonsterBattle game = new MonsterBattle("Peros", "Acos");

		game.setPlayer2Monster(new AirType(), new FireType());
		game.setPlayer1Monster(new EarthType(), new EarthType());

		assertEquals(100, game.getPlayer1().getPlayerMonster().getHealthPoints());
		assertEquals(100, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertEquals("", game.getWinner());

		game.playerAttacks(2);
		game.playerAttacks(2);
		assertEquals(88, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertEquals(79, game.getPlayer1().getPlayerMonster().getHealthPoints());
		assertFalse(game.isOver());

		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(2);
		game.playerAttacks(1);
		game.playerAttacks(1);

		assertEquals(44, game.getPlayer2().getPlayerMonster().getHealthPoints());
		assertEquals(2, game.getPlayer1().getPlayerMonster().getHealthPoints());
		assertFalse(game.isOver());

		game.playerAttacks(1);
		game.playerAttacks(1);

		assertEquals(0, game.getPlayer1().getPlayerMonster().getHealthPoints());
		assertTrue(game.isOver());
		assertEquals("Acos", game.getWinner());

		game.playerAttacks(1);// throws the expected exception, line required to pass the test
	}

	@After
	public void resetPlayerCounter3() {
		Player.resetNumberOfPlayers();
	}

	@Test
	public void setPlayers() {
		MonsterBattle game = new MonsterBattle("olakase", "esakalo");
		Player george = new Player("George");
		Player jorge = new Player("Jorge");

		assertNotEquals(george, game.getPlayer1());
		assertNotEquals(george, game.getPlayer2());
		assertNotEquals(jorge, game.getPlayer1());
		assertNotEquals(jorge, game.getPlayer2());

		game.setPlayer1(george);
		game.setPlayer2(jorge);

		assertEquals(george, game.getPlayer1());
		assertEquals(jorge, game.getPlayer2());
		assertNotEquals(game.getPlayer1(), game.getPlayer2());
	}

	@After
	public void resetPlayerCounter4() {
		Player.resetNumberOfPlayers();
	}

	@Test
	public void createSummary1() throws NoSpecialAttacksLeftException, GameOverException {
		MonsterBattle game = new MonsterBattle("Doberto", "Riego");

		game.setPlayer1Monster(new FireType(), null);
		game.setPlayer2Monster(new WaterType(), new FireType());

		int damageDealt = game.playerAttacks(2);

		assertEquals(
				"Damage dealt: 12\n\nDoberto (fire)\nHealth points left: 100\nSpecial attacks left: 3"
						+ "\n\nRiego (water, fire)\nHealth points left: 88\nSpecial attacks left: 4",
				game.createSummary(damageDealt));
		
		damageDealt = game.playerAttacks(1);
		
		assertEquals(
				"Damage dealt: 12\n\nDoberto (fire)\nHealth points left: 88\nSpecial attacks left: 3"
						+ "\n\nRiego (water, fire)\nHealth points left: 88\nSpecial attacks left: 4",
				game.createSummary(damageDealt));
	}
	
	@Test
	public void createSummary2() throws NoSpecialAttacksLeftException, GameOverException {
		MonsterBattle game = new MonsterBattle("Doberto", "Riego");

		game.setPlayer2Monster(new FireType(), null);
		game.setPlayer1Monster(new WaterType(), new FireType());

		int damageDealt = game.playerAttacks(2);

		assertEquals(
				"Damage dealt: 18\n\nDoberto (water, fire)\nHealth points left: 100\nSpecial attacks left: 3"
						+ "\n\nRiego (fire)\nHealth points left: 82\nSpecial attacks left: 4",
				game.createSummary(damageDealt));
		
		damageDealt = game.playerAttacks(1);
		
		assertEquals(
				"Damage dealt: 8\n\nDoberto (water, fire)\nHealth points left: 92\nSpecial attacks left: 3"
						+ "\n\nRiego (fire)\nHealth points left: 82\nSpecial attacks left: 4",
				game.createSummary(damageDealt));
	}
}
