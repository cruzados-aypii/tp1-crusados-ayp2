import org.junit.Test;

import logicFunctionality.*;

import static org.junit.Assert.*;

public class MonsterTypeTests {

	@Test
	public void createNewFireTypeVisitorAndDefend() {
		FireTypeVisitor fireType = new FireTypeVisitor();

		assertEquals(10, fireType.defend(new FireType(), false));
		assertEquals(15, fireType.defend(new FireType(), true));
		assertEquals(10, fireType.defend(new AirType(), false));
		assertEquals(15, fireType.defend(new AirType(), true));
		assertEquals(12, fireType.defend(new WaterType(), false));
		assertEquals(18, fireType.defend(new WaterType(), true));
		assertEquals(8, fireType.defend(new EarthType(), false));
		assertEquals(12, fireType.defend(new EarthType(), true));
	}

	@Test
	public void createNewWaterTypeVisitorAndDefend() {
		WaterTypeVisitor waterType = new WaterTypeVisitor();

		assertEquals(8, waterType.defend(new FireType(), false));
		assertEquals(12, waterType.defend(new FireType(), true));
		assertEquals(10, waterType.defend(new AirType(), false));
		assertEquals(15, waterType.defend(new AirType(), true));
		assertEquals(10, waterType.defend(new WaterType(), false));
		assertEquals(15, waterType.defend(new WaterType(), true));
		assertEquals(12, waterType.defend(new EarthType(), false));
		assertEquals(18, waterType.defend(new EarthType(), true));
	}

	@Test
	public void createNewAirTypeVisitorAndDefend() {
		AirTypeVisitor airType = new AirTypeVisitor();

		assertEquals(12, airType.defend(new FireType(), false));
		assertEquals(18, airType.defend(new FireType(), true));
		assertEquals(8, airType.defend(new AirType(), false));
		assertEquals(12, airType.defend(new AirType(), true));
		assertEquals(10, airType.defend(new WaterType(), false));
		assertEquals(15, airType.defend(new WaterType(), true));
		assertEquals(10, airType.defend(new EarthType(), false));
		assertEquals(15, airType.defend(new EarthType(), true));
	}

	@Test
	public void createNewEarthTypeVisitorAndDefend() {
		EarthTypeVisitor earthType = new EarthTypeVisitor();

		assertEquals(10, earthType.defend(new FireType(), false));
		assertEquals(15, earthType.defend(new FireType(), true));
		assertEquals(12, earthType.defend(new AirType(), false));
		assertEquals(18, earthType.defend(new AirType(), true));
		assertEquals(8, earthType.defend(new WaterType(), false));
		assertEquals(12, earthType.defend(new WaterType(), true));
		assertEquals(10, earthType.defend(new EarthType(), false));
		assertEquals(15, earthType.defend(new EarthType(), true));
	}

	@Test
	public void createNewFireTypeAndCalculateDamage() {
		FireType fireType = new FireType();

		assertEquals(10, fireType.calculateDamage(new FireTypeVisitor(), false));
		assertEquals(15, fireType.calculateDamage(new FireTypeVisitor(), true));
		assertEquals(8, fireType.calculateDamage(new WaterTypeVisitor(), false));
		assertEquals(12, fireType.calculateDamage(new WaterTypeVisitor(), true));
		assertEquals(12, fireType.calculateDamage(new AirTypeVisitor(), false));
		assertEquals(18, fireType.calculateDamage(new AirTypeVisitor(), true));
		assertEquals(10, fireType.calculateDamage(new EarthTypeVisitor(), false));
		assertEquals(15, fireType.calculateDamage(new EarthTypeVisitor(), true));
	}

	@Test
	public void createNewWaterTypeAndCalculateDamage() {
		WaterType waterType = new WaterType();

		assertEquals(12, waterType.calculateDamage(new FireTypeVisitor(), false));
		assertEquals(18, waterType.calculateDamage(new FireTypeVisitor(), true));
		assertEquals(10, waterType.calculateDamage(new WaterTypeVisitor(), false));
		assertEquals(15, waterType.calculateDamage(new WaterTypeVisitor(), true));
		assertEquals(10, waterType.calculateDamage(new AirTypeVisitor(), false));
		assertEquals(15, waterType.calculateDamage(new AirTypeVisitor(), true));
		assertEquals(8, waterType.calculateDamage(new EarthTypeVisitor(), false));
		assertEquals(12, waterType.calculateDamage(new EarthTypeVisitor(), true));
	}

	@Test
	public void createNewAirTypeAndCalculateDamage() {
		AirType airType = new AirType();

		assertEquals(10, airType.calculateDamage(new FireTypeVisitor(), false));
		assertEquals(15, airType.calculateDamage(new FireTypeVisitor(), true));
		assertEquals(10, airType.calculateDamage(new WaterTypeVisitor(), false));
		assertEquals(15, airType.calculateDamage(new WaterTypeVisitor(), true));
		assertEquals(8, airType.calculateDamage(new AirTypeVisitor(), false));
		assertEquals(12, airType.calculateDamage(new AirTypeVisitor(), true));
		assertEquals(12, airType.calculateDamage(new EarthTypeVisitor(), false));
		assertEquals(18, airType.calculateDamage(new EarthTypeVisitor(), true));
	}

	@Test
	public void createNewEarthTypeAndCalculateDamage() {
		EarthType earthType = new EarthType();

		assertEquals(8, earthType.calculateDamage(new FireTypeVisitor(), false));
		assertEquals(12, earthType.calculateDamage(new FireTypeVisitor(), true));
		assertEquals(12, earthType.calculateDamage(new WaterTypeVisitor(), false));
		assertEquals(18, earthType.calculateDamage(new WaterTypeVisitor(), true));
		assertEquals(10, earthType.calculateDamage(new AirTypeVisitor(), false));
		assertEquals(15, earthType.calculateDamage(new AirTypeVisitor(), true));
		assertEquals(10, earthType.calculateDamage(new EarthTypeVisitor(), false));
		assertEquals(15, earthType.calculateDamage(new EarthTypeVisitor(), true));
	}
	
	@Test
	public void toStringOutput() {
		EarthType earth = new EarthType();
		AirType air = new AirType();
		FireType fire = new FireType();
		WaterType water = new WaterType();
		
		assertEquals("earth", earth.toString());
		assertEquals("air", air.toString());
		assertEquals("fire", fire.toString());
		assertEquals("water", water.toString());
	}
}
