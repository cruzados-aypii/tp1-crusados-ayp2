import org.junit.Test;

import logicFunctionality.*;

import static org.junit.Assert.*;

public class MonsterTests {

	@Test
	public void createNewMonsterGetHealthPointsAndSpecialAttacks() {
		Monster aNewMonster = new Monster(new FireType(), new AirType());

		assertEquals(100, aNewMonster.getHealthPoints());
		assertEquals(4, aNewMonster.getSpecialAttacksLeft());
	}

	@Test
	public void createMonsterWithOneType() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new FireType());

		assertTrue(monster.getTypeX() instanceof FireType);
		assertNull(monster.getTypeY());

		Monster enemyMonster = new Monster(new FireType(), new WaterType());

		assertEquals(10, enemyMonster.attack(monster, 1));
		assertEquals(90, monster.getHealthPoints());
		assertEquals(8, monster.attack(enemyMonster, 1));
		assertEquals(92, enemyMonster.getHealthPoints());

		Monster anotherMonster = new Monster(new WaterType());

		assertEquals(8, monster.attack(anotherMonster, 1));
		assertEquals(92, anotherMonster.getHealthPoints());
	}
	


	@Test
	public void FireAttacksAsFirstType() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new FireType(), new EarthType());

		Monster waterAir = new Monster(new WaterType(), new AirType());
		Monster waterFire = new Monster(new WaterType(), new FireType());
		Monster waterEarth = new Monster(new WaterType(), new EarthType());
		Monster waterWater = new Monster(new WaterType(), new WaterType());
		Monster fireAir = new Monster(new FireType(), new AirType());
		Monster fireWater = new Monster(new FireType(), new WaterType());
		Monster fireEarth = new Monster(new FireType(), new EarthType());
		Monster fireFire = new Monster(new FireType(), new FireType());
		Monster earthAir = new Monster(new EarthType(), new AirType());
		Monster earthWater = new Monster(new EarthType(), new WaterType());
		Monster earthFire = new Monster(new EarthType(), new FireType());
		Monster earthEarth = new Monster(new EarthType(), new EarthType());
		Monster airAir = new Monster(new AirType(), new AirType());
		Monster airEarth = new Monster(new AirType(), new EarthType());
		Monster airFire = new Monster(new AirType(), new FireType());
		Monster airWater = new Monster(new AirType(), new WaterType());
		Monster air = new Monster(new AirType());
		Monster fire = new Monster(new FireType());
		Monster earth = new Monster(new EarthType());
		Monster water = new Monster(new WaterType());
		
		monster.setSpecialAttacksLeft(20); //prevent NoSpecialAttacksLeftException

		assertEquals(12, monster.attack(air, 1));
		assertEquals(88, air.getHealthPoints());
		assertEquals(18, monster.attack(air, 2));
		assertEquals(70, air.getHealthPoints());
		
		assertEquals(10, monster.attack(fire, 1));
		assertEquals(90, fire.getHealthPoints());
		assertEquals(15, monster.attack(fire, 2));
		assertEquals(75, fire.getHealthPoints());
		
		assertEquals(10, monster.attack(earth, 1));
		assertEquals(90, earth.getHealthPoints());
		assertEquals(15, monster.attack(earth, 2));
		assertEquals(75, earth.getHealthPoints());
		
		assertEquals(8, monster.attack(water, 1));
		assertEquals(92, water.getHealthPoints());
		assertEquals(12, monster.attack(water, 2));
		assertEquals(80, water.getHealthPoints());
		
		assertEquals(10, monster.attack(waterAir, 1));
		assertEquals(90, waterAir.getHealthPoints());
		assertEquals(15, monster.attack(waterAir, 2));
		assertEquals(75, waterAir.getHealthPoints());

		assertEquals(8, monster.attack(waterFire, 1));
		assertEquals(92, waterFire.getHealthPoints());
		assertEquals(12, monster.attack(waterFire, 2));
		assertEquals(80, waterFire.getHealthPoints());

		assertEquals(8, monster.attack(waterEarth, 1));
		assertEquals(92, waterEarth.getHealthPoints());
		assertEquals(12, monster.attack(waterEarth, 2));
		assertEquals(80, waterEarth.getHealthPoints());

		assertEquals(6, monster.attack(waterWater, 1));
		assertEquals(94, waterWater.getHealthPoints());
		assertEquals(9, monster.attack(waterWater, 2));
		assertEquals(85, waterWater.getHealthPoints());

		assertEquals(12, monster.attack(fireAir, 1));
		assertEquals(88, fireAir.getHealthPoints());
		assertEquals(18, monster.attack(fireAir, 2));
		assertEquals(70, fireAir.getHealthPoints());

		assertEquals(8, monster.attack(fireWater, 1));
		assertEquals(92, fireWater.getHealthPoints());
		assertEquals(12, monster.attack(fireWater, 2));
		assertEquals(80, fireWater.getHealthPoints());

		assertEquals(10, monster.attack(fireEarth, 1));
		assertEquals(90, fireEarth.getHealthPoints());
		assertEquals(15, monster.attack(fireEarth, 2));
		assertEquals(75, fireEarth.getHealthPoints());

		assertEquals(10, monster.attack(fireFire, 1));
		assertEquals(90, fireFire.getHealthPoints());
		assertEquals(15, monster.attack(fireFire, 2));
		assertEquals(75, fireFire.getHealthPoints());

		assertEquals(12, monster.attack(earthAir, 1));
		assertEquals(88, earthAir.getHealthPoints());
		assertEquals(18, monster.attack(earthAir, 2));
		assertEquals(70, earthAir.getHealthPoints());

		assertEquals(8, monster.attack(earthWater, 1));
		assertEquals(92, earthWater.getHealthPoints());
		assertEquals(12, monster.attack(earthWater, 2));
		assertEquals(80, earthWater.getHealthPoints());

		assertEquals(10, monster.attack(earthFire, 1));
		assertEquals(90, earthFire.getHealthPoints());
		assertEquals(15, monster.attack(earthFire, 2));
		assertEquals(75, earthFire.getHealthPoints());

		assertEquals(10, monster.attack(earthEarth, 1));
		assertEquals(90, earthEarth.getHealthPoints());
		assertEquals(15, monster.attack(earthEarth, 2));
		assertEquals(75, earthEarth.getHealthPoints());

		assertEquals(14, monster.attack(airAir, 1));
		assertEquals(86, airAir.getHealthPoints());
		assertEquals(21, monster.attack(airAir, 2));
		assertEquals(65, airAir.getHealthPoints());

		assertEquals(12, monster.attack(airEarth, 1));
		assertEquals(88, airEarth.getHealthPoints());
		assertEquals(18, monster.attack(airEarth, 2));
		assertEquals(70, airEarth.getHealthPoints());

		assertEquals(12, monster.attack(airFire, 1));
		assertEquals(88, airFire.getHealthPoints());
		assertEquals(18, monster.attack(airFire, 2));
		assertEquals(70, airFire.getHealthPoints());

		assertEquals(10, monster.attack(airWater, 1));
		assertEquals(90, airWater.getHealthPoints());
		assertEquals(15, monster.attack(airWater, 2));
		assertEquals(75, airWater.getHealthPoints());
	}

	@Test
	public void WaterAttacksAsFirstType() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new WaterType(), new EarthType());

		Monster waterAir = new Monster(new WaterType(), new AirType());
		Monster waterFire = new Monster(new WaterType(), new FireType());
		Monster waterEarth = new Monster(new WaterType(), new EarthType());
		Monster waterWater = new Monster(new WaterType(), new WaterType());
		Monster fireAir = new Monster(new FireType(), new AirType());
		Monster fireWater = new Monster(new FireType(), new WaterType());
		Monster fireEarth = new Monster(new FireType(), new EarthType());
		Monster fireFire = new Monster(new FireType(), new FireType());
		Monster earthAir = new Monster(new EarthType(), new AirType());
		Monster earthWater = new Monster(new EarthType(), new WaterType());
		Monster earthFire = new Monster(new EarthType(), new FireType());
		Monster earthEarth = new Monster(new EarthType(), new EarthType());
		Monster airAir = new Monster(new AirType(), new AirType());
		Monster airEarth = new Monster(new AirType(), new EarthType());
		Monster airFire = new Monster(new AirType(), new FireType());
		Monster airWater = new Monster(new AirType(), new WaterType());
		Monster air = new Monster(new AirType());
		Monster fire = new Monster(new FireType());
		Monster earth = new Monster(new EarthType());
		Monster water = new Monster(new WaterType());
		
		monster.setSpecialAttacksLeft(20); //prevent NoSpecialAttacksLeftException

		assertEquals(10, monster.attack(air, 1));
		assertEquals(90, air.getHealthPoints());
		assertEquals(15, monster.attack(air, 2));
		assertEquals(75, air.getHealthPoints());
		
		assertEquals(12, monster.attack(fire, 1));
		assertEquals(88, fire.getHealthPoints());
		assertEquals(18, monster.attack(fire, 2));
		assertEquals(70, fire.getHealthPoints());
		
		assertEquals(8, monster.attack(earth, 1));
		assertEquals(92, earth.getHealthPoints());
		assertEquals(12, monster.attack(earth, 2));
		assertEquals(80, earth.getHealthPoints());
		
		assertEquals(10, monster.attack(water, 1));
		assertEquals(90, water.getHealthPoints());
		assertEquals(15, monster.attack(water, 2));
		assertEquals(75, water.getHealthPoints());
		
		assertEquals(10, monster.attack(waterAir, 1));
		assertEquals(90, waterAir.getHealthPoints());
		assertEquals(15, monster.attack(waterAir, 2));
		assertEquals(75, waterAir.getHealthPoints());

		assertEquals(12, monster.attack(waterFire, 1));
		assertEquals(88, waterFire.getHealthPoints());
		assertEquals(18, monster.attack(waterFire, 2));
		assertEquals(70, waterFire.getHealthPoints());

		assertEquals(8, monster.attack(waterEarth, 1));
		assertEquals(92, waterEarth.getHealthPoints());
		assertEquals(12, monster.attack(waterEarth, 2));
		assertEquals(80, waterEarth.getHealthPoints());

		assertEquals(10, monster.attack(waterWater, 1));
		assertEquals(90, waterWater.getHealthPoints());
		assertEquals(15, monster.attack(waterWater, 2));
		assertEquals(75, waterWater.getHealthPoints());

		assertEquals(12, monster.attack(fireAir, 1));
		assertEquals(88, fireAir.getHealthPoints());
		assertEquals(18, monster.attack(fireAir, 2));
		assertEquals(70, fireAir.getHealthPoints());

		assertEquals(12, monster.attack(fireWater, 1));
		assertEquals(88, fireWater.getHealthPoints());
		assertEquals(18, monster.attack(fireWater, 2));
		assertEquals(70, fireWater.getHealthPoints());

		assertEquals(10, monster.attack(fireEarth, 1));
		assertEquals(90, fireEarth.getHealthPoints());
		assertEquals(15, monster.attack(fireEarth, 2));
		assertEquals(75, fireEarth.getHealthPoints());

		assertEquals(14, monster.attack(fireFire, 1));
		assertEquals(86, fireFire.getHealthPoints());
		assertEquals(21, monster.attack(fireFire, 2));
		assertEquals(65, fireFire.getHealthPoints());

		assertEquals(8, monster.attack(earthAir, 1));
		assertEquals(92, earthAir.getHealthPoints());
		assertEquals(12, monster.attack(earthAir, 2));
		assertEquals(80, earthAir.getHealthPoints());

		assertEquals(8, monster.attack(earthWater, 1));
		assertEquals(92, earthWater.getHealthPoints());
		assertEquals(12, monster.attack(earthWater, 2));
		assertEquals(80, earthWater.getHealthPoints());

		assertEquals(10, monster.attack(earthFire, 1));
		assertEquals(90, earthFire.getHealthPoints());
		assertEquals(15, monster.attack(earthFire, 2));
		assertEquals(75, earthFire.getHealthPoints());

		assertEquals(6, monster.attack(earthEarth, 1));
		assertEquals(94, earthEarth.getHealthPoints());
		assertEquals(9, monster.attack(earthEarth, 2));
		assertEquals(85, earthEarth.getHealthPoints());

		assertEquals(10, monster.attack(airAir, 1));
		assertEquals(90, airAir.getHealthPoints());
		assertEquals(15, monster.attack(airAir, 2));
		assertEquals(75, airAir.getHealthPoints());

		assertEquals(8, monster.attack(airEarth, 1));
		assertEquals(92, airEarth.getHealthPoints());
		assertEquals(12, monster.attack(airEarth, 2));
		assertEquals(80, airEarth.getHealthPoints());

		assertEquals(12, monster.attack(airFire, 1));
		assertEquals(88, airFire.getHealthPoints());
		assertEquals(18, monster.attack(airFire, 2));
		assertEquals(70, airFire.getHealthPoints());

		assertEquals(10, monster.attack(airWater, 1));
		assertEquals(90, airWater.getHealthPoints());
		assertEquals(15, monster.attack(airWater, 2));
		assertEquals(75, airWater.getHealthPoints());
	}

	@Test
	public void AirAttacksAsFirstType() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new AirType(), new EarthType());

		Monster waterAir = new Monster(new WaterType(), new AirType());
		Monster waterFire = new Monster(new WaterType(), new FireType());
		Monster waterEarth = new Monster(new WaterType(), new EarthType());
		Monster waterWater = new Monster(new WaterType(), new WaterType());
		Monster fireAir = new Monster(new FireType(), new AirType());
		Monster fireWater = new Monster(new FireType(), new WaterType());
		Monster fireEarth = new Monster(new FireType(), new EarthType());
		Monster fireFire = new Monster(new FireType(), new FireType());
		Monster earthAir = new Monster(new EarthType(), new AirType());
		Monster earthWater = new Monster(new EarthType(), new WaterType());
		Monster earthFire = new Monster(new EarthType(), new FireType());
		Monster earthEarth = new Monster(new EarthType(), new EarthType());
		Monster airAir = new Monster(new AirType(), new AirType());
		Monster airEarth = new Monster(new AirType(), new EarthType());
		Monster airFire = new Monster(new AirType(), new FireType());
		Monster airWater = new Monster(new AirType(), new WaterType());
		Monster air = new Monster(new AirType());
		Monster fire = new Monster(new FireType());
		Monster earth = new Monster(new EarthType());
		Monster water = new Monster(new WaterType());
		
		monster.setSpecialAttacksLeft(20); //prevent NoSpecialAttacksLeftException

		assertEquals(8, monster.attack(air, 1));
		assertEquals(92, air.getHealthPoints());
		assertEquals(12, monster.attack(air, 2));
		assertEquals(80, air.getHealthPoints());
		
		assertEquals(10, monster.attack(fire, 1));
		assertEquals(90, fire.getHealthPoints());
		assertEquals(15, monster.attack(fire, 2));
		assertEquals(75, fire.getHealthPoints());
		
		assertEquals(12, monster.attack(earth, 1));
		assertEquals(88, earth.getHealthPoints());
		assertEquals(18, monster.attack(earth, 2));
		assertEquals(70, earth.getHealthPoints());
		
		assertEquals(10, monster.attack(water, 1));
		assertEquals(90, water.getHealthPoints());
		assertEquals(15, monster.attack(water, 2));
		assertEquals(75, water.getHealthPoints());

		assertEquals(8, monster.attack(waterAir, 1));
		assertEquals(92, waterAir.getHealthPoints());
		assertEquals(12, monster.attack(waterAir, 2));
		assertEquals(80, waterAir.getHealthPoints());

		assertEquals(10, monster.attack(waterFire, 1));
		assertEquals(90, waterFire.getHealthPoints());
		assertEquals(15, monster.attack(waterFire, 2));
		assertEquals(75, waterFire.getHealthPoints());

		assertEquals(12, monster.attack(waterEarth, 1));
		assertEquals(88, waterEarth.getHealthPoints());
		assertEquals(18, monster.attack(waterEarth, 2));
		assertEquals(70, waterEarth.getHealthPoints());

		assertEquals(10, monster.attack(waterWater, 1));
		assertEquals(90, waterWater.getHealthPoints());
		assertEquals(15, monster.attack(waterWater, 2));
		assertEquals(75, waterWater.getHealthPoints());

		assertEquals(8, monster.attack(fireAir, 1));
		assertEquals(92, fireAir.getHealthPoints());
		assertEquals(12, monster.attack(fireAir, 2));
		assertEquals(80, fireAir.getHealthPoints());

		assertEquals(10, monster.attack(fireWater, 1));
		assertEquals(90, fireWater.getHealthPoints());
		assertEquals(15, monster.attack(fireWater, 2));
		assertEquals(75, fireWater.getHealthPoints());

		assertEquals(12, monster.attack(fireEarth, 1));
		assertEquals(88, fireEarth.getHealthPoints());
		assertEquals(18, monster.attack(fireEarth, 2));
		assertEquals(70, fireEarth.getHealthPoints());

		assertEquals(10, monster.attack(fireFire, 1));
		assertEquals(90, fireFire.getHealthPoints());
		assertEquals(15, monster.attack(fireFire, 2));
		assertEquals(75, fireFire.getHealthPoints());

		assertEquals(10, monster.attack(earthAir, 1));
		assertEquals(90, earthAir.getHealthPoints());
		assertEquals(15, monster.attack(earthAir, 2));
		assertEquals(75, earthAir.getHealthPoints());

		assertEquals(12, monster.attack(earthWater, 1));
		assertEquals(88, earthWater.getHealthPoints());
		assertEquals(18, monster.attack(earthWater, 2));
		assertEquals(70, earthWater.getHealthPoints());

		assertEquals(12, monster.attack(earthFire, 1));
		assertEquals(88, earthFire.getHealthPoints());
		assertEquals(18, monster.attack(earthFire, 2));
		assertEquals(70, earthFire.getHealthPoints());

		assertEquals(14, monster.attack(earthEarth, 1));
		assertEquals(86, earthEarth.getHealthPoints());
		assertEquals(21, monster.attack(earthEarth, 2));
		assertEquals(65, earthEarth.getHealthPoints());

		assertEquals(6, monster.attack(airAir, 1));
		assertEquals(94, airAir.getHealthPoints());
		assertEquals(9, monster.attack(airAir, 2));
		assertEquals(85, airAir.getHealthPoints());

		assertEquals(10, monster.attack(airEarth, 1));
		assertEquals(90, airEarth.getHealthPoints());
		assertEquals(15, monster.attack(airEarth, 2));
		assertEquals(75, airEarth.getHealthPoints());

		assertEquals(8, monster.attack(airFire, 1));
		assertEquals(92, airFire.getHealthPoints());
		assertEquals(12, monster.attack(airFire, 2));
		assertEquals(80, airFire.getHealthPoints());

		assertEquals(8, monster.attack(airWater, 1));
		assertEquals(92, airWater.getHealthPoints());
		assertEquals(12, monster.attack(airWater, 2));
		assertEquals(80, airWater.getHealthPoints());
	}

	@Test
	public void EarthAttacksAsFirstType() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new EarthType(), new EarthType());

		Monster waterAir = new Monster(new WaterType(), new AirType());
		Monster waterFire = new Monster(new WaterType(), new FireType());
		Monster waterEarth = new Monster(new WaterType(), new EarthType());
		Monster waterWater = new Monster(new WaterType(), new WaterType());
		Monster fireAir = new Monster(new FireType(), new AirType());
		Monster fireWater = new Monster(new FireType(), new WaterType());
		Monster fireEarth = new Monster(new FireType(), new EarthType());
		Monster fireFire = new Monster(new FireType(), new FireType());
		Monster earthAir = new Monster(new EarthType(), new AirType());
		Monster earthWater = new Monster(new EarthType(), new WaterType());
		Monster earthFire = new Monster(new EarthType(), new FireType());
		Monster earthEarth = new Monster(new EarthType(), new EarthType());
		Monster airAir = new Monster(new AirType(), new AirType());
		Monster airEarth = new Monster(new AirType(), new EarthType());
		Monster airFire = new Monster(new AirType(), new FireType());
		Monster airWater = new Monster(new AirType(), new WaterType());
		Monster air = new Monster(new AirType());
		Monster fire = new Monster(new FireType());
		Monster earth = new Monster(new EarthType());
		Monster water = new Monster(new WaterType());
		
		monster.setSpecialAttacksLeft(20); //prevent NoSpecialAttacksLeftException

		assertEquals(10, monster.attack(air, 1));
		assertEquals(90, air.getHealthPoints());
		assertEquals(15, monster.attack(air, 2));
		assertEquals(75, air.getHealthPoints());
		
		assertEquals(8, monster.attack(fire, 1));
		assertEquals(92, fire.getHealthPoints());
		assertEquals(12, monster.attack(fire, 2));
		assertEquals(80, fire.getHealthPoints());
		
		assertEquals(10, monster.attack(earth, 1));
		assertEquals(90, earth.getHealthPoints());
		assertEquals(15, monster.attack(earth, 2));
		assertEquals(75, earth.getHealthPoints());
		
		assertEquals(12, monster.attack(water, 1));
		assertEquals(88, water.getHealthPoints());
		assertEquals(18, monster.attack(water, 2));
		assertEquals(70, water.getHealthPoints());

		assertEquals(12, monster.attack(waterAir, 1));
		assertEquals(88, waterAir.getHealthPoints());
		assertEquals(18, monster.attack(waterAir, 2));
		assertEquals(70, waterAir.getHealthPoints());

		assertEquals(10, monster.attack(waterFire, 1));
		assertEquals(90, waterFire.getHealthPoints());
		assertEquals(15, monster.attack(waterFire, 2));
		assertEquals(75, waterFire.getHealthPoints());

		assertEquals(12, monster.attack(waterEarth, 1));
		assertEquals(88, waterEarth.getHealthPoints());
		assertEquals(18, monster.attack(waterEarth, 2));
		assertEquals(70, waterEarth.getHealthPoints());

		assertEquals(14, monster.attack(waterWater, 1));
		assertEquals(86, waterWater.getHealthPoints());
		assertEquals(21, monster.attack(waterWater, 2));
		assertEquals(65, waterWater.getHealthPoints());

		assertEquals(8, monster.attack(fireAir, 1));
		assertEquals(92, fireAir.getHealthPoints());
		assertEquals(12, monster.attack(fireAir, 2));
		assertEquals(80, fireAir.getHealthPoints());

		assertEquals(10, monster.attack(fireWater, 1));
		assertEquals(90, fireWater.getHealthPoints());
		assertEquals(15, monster.attack(fireWater, 2));
		assertEquals(75, fireWater.getHealthPoints());

		assertEquals(8, monster.attack(fireEarth, 1));
		assertEquals(92, fireEarth.getHealthPoints());
		assertEquals(12, monster.attack(fireEarth, 2));
		assertEquals(80, fireEarth.getHealthPoints());

		assertEquals(6, monster.attack(fireFire, 1));
		assertEquals(94, fireFire.getHealthPoints());
		assertEquals(9, monster.attack(fireFire, 2));
		assertEquals(85, fireFire.getHealthPoints());

		assertEquals(10, monster.attack(earthAir, 1));
		assertEquals(90, earthAir.getHealthPoints());
		assertEquals(15, monster.attack(earthAir, 2));
		assertEquals(75, earthAir.getHealthPoints());

		assertEquals(12, monster.attack(earthWater, 1));
		assertEquals(88, earthWater.getHealthPoints());
		assertEquals(18, monster.attack(earthWater, 2));
		assertEquals(70, earthWater.getHealthPoints());

		assertEquals(8, monster.attack(earthFire, 1));
		assertEquals(92, earthFire.getHealthPoints());
		assertEquals(12, monster.attack(earthFire, 2));
		assertEquals(80, earthFire.getHealthPoints());

		assertEquals(10, monster.attack(earthEarth, 1));
		assertEquals(90, earthEarth.getHealthPoints());
		assertEquals(15, monster.attack(earthEarth, 2));
		assertEquals(75, earthEarth.getHealthPoints());

		assertEquals(10, monster.attack(airAir, 1));
		assertEquals(90, airAir.getHealthPoints());
		assertEquals(15, monster.attack(airAir, 2));
		assertEquals(75, airAir.getHealthPoints());

		assertEquals(10, monster.attack(airEarth, 1));
		assertEquals(90, airEarth.getHealthPoints());
		assertEquals(15, monster.attack(airEarth, 2));
		assertEquals(75, airEarth.getHealthPoints());

		assertEquals(8, monster.attack(airFire, 1));
		assertEquals(92, airFire.getHealthPoints());
		assertEquals(12, monster.attack(airFire, 2));
		assertEquals(80, airFire.getHealthPoints());

		assertEquals(12, monster.attack(airWater, 1));
		assertEquals(88, airWater.getHealthPoints());
		assertEquals(18, monster.attack(airWater, 2));
		assertEquals(70, airWater.getHealthPoints());
	}
	
	@Test
	public void FireAttacksAsSecondType() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new EarthType(), new FireType());

		Monster waterAir = new Monster(new WaterType(), new AirType());
		Monster waterFire = new Monster(new WaterType(), new FireType());
		Monster waterEarth = new Monster(new WaterType(), new EarthType());
		Monster waterWater = new Monster(new WaterType(), new WaterType());
		Monster fireAir = new Monster(new FireType(), new AirType());
		Monster fireWater = new Monster(new FireType(), new WaterType());
		Monster fireEarth = new Monster(new FireType(), new EarthType());
		Monster fireFire = new Monster(new FireType(), new FireType());
		Monster earthAir = new Monster(new EarthType(), new AirType());
		Monster earthWater = new Monster(new EarthType(), new WaterType());
		Monster earthFire = new Monster(new EarthType(), new FireType());
		Monster earthEarth = new Monster(new EarthType(), new EarthType());
		Monster airAir = new Monster(new AirType(), new AirType());
		Monster airEarth = new Monster(new AirType(), new EarthType());
		Monster airFire = new Monster(new AirType(), new FireType());
		Monster airWater = new Monster(new AirType(), new WaterType());
		Monster air = new Monster(new AirType());
		Monster fire = new Monster(new FireType());
		Monster earth = new Monster(new EarthType());
		Monster water = new Monster(new WaterType());
		
		monster.setSpecialAttacksLeft(20); //prevent NoSpecialAttacksLeftException

		assertEquals(12, monster.attack(air, 3));
		assertEquals(88, air.getHealthPoints());
		assertEquals(18, monster.attack(air, 4));
		assertEquals(70, air.getHealthPoints());
		
		assertEquals(10, monster.attack(fire, 3));
		assertEquals(90, fire.getHealthPoints());
		assertEquals(15, monster.attack(fire, 4));
		assertEquals(75, fire.getHealthPoints());
		
		assertEquals(10, monster.attack(earth, 3));
		assertEquals(90, earth.getHealthPoints());
		assertEquals(15, monster.attack(earth, 4));
		assertEquals(75, earth.getHealthPoints());
		
		assertEquals(8, monster.attack(water, 3));
		assertEquals(92, water.getHealthPoints());
		assertEquals(12, monster.attack(water, 4));
		assertEquals(80, water.getHealthPoints());

		assertEquals(10, monster.attack(waterAir, 3));
		assertEquals(90, waterAir.getHealthPoints());
		assertEquals(15, monster.attack(waterAir, 4));
		assertEquals(75, waterAir.getHealthPoints());

		assertEquals(8, monster.attack(waterFire, 3));
		assertEquals(92, waterFire.getHealthPoints());
		assertEquals(12, monster.attack(waterFire, 4));
		assertEquals(80, waterFire.getHealthPoints());

		assertEquals(8, monster.attack(waterEarth, 3));
		assertEquals(92, waterEarth.getHealthPoints());
		assertEquals(12, monster.attack(waterEarth, 4));
		assertEquals(80, waterEarth.getHealthPoints());

		assertEquals(6, monster.attack(waterWater, 3));
		assertEquals(94, waterWater.getHealthPoints());
		assertEquals(9, monster.attack(waterWater, 4));
		assertEquals(85, waterWater.getHealthPoints());

		assertEquals(12, monster.attack(fireAir, 3));
		assertEquals(88, fireAir.getHealthPoints());
		assertEquals(18, monster.attack(fireAir, 4));
		assertEquals(70, fireAir.getHealthPoints());

		assertEquals(8, monster.attack(fireWater, 3));
		assertEquals(92, fireWater.getHealthPoints());
		assertEquals(12, monster.attack(fireWater, 4));
		assertEquals(80, fireWater.getHealthPoints());

		assertEquals(10, monster.attack(fireEarth, 3));
		assertEquals(90, fireEarth.getHealthPoints());
		assertEquals(15, monster.attack(fireEarth, 4));
		assertEquals(75, fireEarth.getHealthPoints());

		assertEquals(10, monster.attack(fireFire, 3));
		assertEquals(90, fireFire.getHealthPoints());
		assertEquals(15, monster.attack(fireFire, 4));
		assertEquals(75, fireFire.getHealthPoints());

		assertEquals(12, monster.attack(earthAir, 3));
		assertEquals(88, earthAir.getHealthPoints());
		assertEquals(18, monster.attack(earthAir, 4));
		assertEquals(70, earthAir.getHealthPoints());

		assertEquals(8, monster.attack(earthWater, 3));
		assertEquals(92, earthWater.getHealthPoints());
		assertEquals(12, monster.attack(earthWater, 4));
		assertEquals(80, earthWater.getHealthPoints());

		assertEquals(10, monster.attack(earthFire, 3));
		assertEquals(90, earthFire.getHealthPoints());
		assertEquals(15, monster.attack(earthFire, 4));
		assertEquals(75, earthFire.getHealthPoints());

		assertEquals(10, monster.attack(earthEarth, 3));
		assertEquals(90, earthEarth.getHealthPoints());
		assertEquals(15, monster.attack(earthEarth, 4));
		assertEquals(75, earthEarth.getHealthPoints());

		assertEquals(14, monster.attack(airAir, 3));
		assertEquals(86, airAir.getHealthPoints());
		assertEquals(21, monster.attack(airAir, 4));
		assertEquals(65, airAir.getHealthPoints());

		assertEquals(12, monster.attack(airEarth, 3));
		assertEquals(88, airEarth.getHealthPoints());
		assertEquals(18, monster.attack(airEarth, 4));
		assertEquals(70, airEarth.getHealthPoints());

		assertEquals(12, monster.attack(airFire, 3));
		assertEquals(88, airFire.getHealthPoints());
		assertEquals(18, monster.attack(airFire, 4));
		assertEquals(70, airFire.getHealthPoints());

		assertEquals(10, monster.attack(airWater, 3));
		assertEquals(90, airWater.getHealthPoints());
		assertEquals(15, monster.attack(airWater, 4));
		assertEquals(75, airWater.getHealthPoints());
	}

	@Test
	public void WaterAttacksAsSecondType() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new AirType(), new WaterType());

		Monster waterAir = new Monster(new WaterType(), new AirType());
		Monster waterFire = new Monster(new WaterType(), new FireType());
		Monster waterEarth = new Monster(new WaterType(), new EarthType());
		Monster waterWater = new Monster(new WaterType(), new WaterType());
		Monster fireAir = new Monster(new FireType(), new AirType());
		Monster fireWater = new Monster(new FireType(), new WaterType());
		Monster fireEarth = new Monster(new FireType(), new EarthType());
		Monster fireFire = new Monster(new FireType(), new FireType());
		Monster earthAir = new Monster(new EarthType(), new AirType());
		Monster earthWater = new Monster(new EarthType(), new WaterType());
		Monster earthFire = new Monster(new EarthType(), new FireType());
		Monster earthEarth = new Monster(new EarthType(), new EarthType());
		Monster airAir = new Monster(new AirType(), new AirType());
		Monster airEarth = new Monster(new AirType(), new EarthType());
		Monster airFire = new Monster(new AirType(), new FireType());
		Monster airWater = new Monster(new AirType(), new WaterType());
		Monster air = new Monster(new AirType());
		Monster fire = new Monster(new FireType());
		Monster earth = new Monster(new EarthType());
		Monster water = new Monster(new WaterType());
		
		monster.setSpecialAttacksLeft(20); //prevent NoSpecialAttacksLeftException

		assertEquals(10, monster.attack(air, 3));
		assertEquals(90, air.getHealthPoints());
		assertEquals(15, monster.attack(air, 4));
		assertEquals(75, air.getHealthPoints());
		
		assertEquals(12, monster.attack(fire, 3));
		assertEquals(88, fire.getHealthPoints());
		assertEquals(18, monster.attack(fire, 4));
		assertEquals(70, fire.getHealthPoints());
		
		assertEquals(8, monster.attack(earth, 3));
		assertEquals(92, earth.getHealthPoints());
		assertEquals(12, monster.attack(earth, 4));
		assertEquals(80, earth.getHealthPoints());
		
		assertEquals(10, monster.attack(water, 3));
		assertEquals(90, water.getHealthPoints());
		assertEquals(15, monster.attack(water, 4));
		assertEquals(75, water.getHealthPoints());

		assertEquals(10, monster.attack(waterAir, 3));
		assertEquals(90, waterAir.getHealthPoints());
		assertEquals(15, monster.attack(waterAir, 4));
		assertEquals(75, waterAir.getHealthPoints());

		assertEquals(12, monster.attack(waterFire, 3));
		assertEquals(88, waterFire.getHealthPoints());
		assertEquals(18, monster.attack(waterFire, 4));
		assertEquals(70, waterFire.getHealthPoints());

		assertEquals(8, monster.attack(waterEarth, 3));
		assertEquals(92, waterEarth.getHealthPoints());
		assertEquals(12, monster.attack(waterEarth, 4));
		assertEquals(80, waterEarth.getHealthPoints());

		assertEquals(10, monster.attack(waterWater, 3));
		assertEquals(90, waterWater.getHealthPoints());
		assertEquals(15, monster.attack(waterWater, 4));
		assertEquals(75, waterWater.getHealthPoints());

		assertEquals(12, monster.attack(fireAir, 3));
		assertEquals(88, fireAir.getHealthPoints());
		assertEquals(18, monster.attack(fireAir, 4));
		assertEquals(70, fireAir.getHealthPoints());

		assertEquals(12, monster.attack(fireWater, 3));
		assertEquals(88, fireWater.getHealthPoints());
		assertEquals(18, monster.attack(fireWater, 4));
		assertEquals(70, fireWater.getHealthPoints());

		assertEquals(10, monster.attack(fireEarth, 3));
		assertEquals(90, fireEarth.getHealthPoints());
		assertEquals(15, monster.attack(fireEarth, 4));
		assertEquals(75, fireEarth.getHealthPoints());

		assertEquals(14, monster.attack(fireFire, 3));
		assertEquals(86, fireFire.getHealthPoints());
		assertEquals(21, monster.attack(fireFire, 4));
		assertEquals(65, fireFire.getHealthPoints());

		assertEquals(8, monster.attack(earthAir, 3));
		assertEquals(92, earthAir.getHealthPoints());
		assertEquals(12, monster.attack(earthAir, 4));
		assertEquals(80, earthAir.getHealthPoints());

		assertEquals(8, monster.attack(earthWater, 3));
		assertEquals(92, earthWater.getHealthPoints());
		assertEquals(12, monster.attack(earthWater, 4));
		assertEquals(80, earthWater.getHealthPoints());

		assertEquals(10, monster.attack(earthFire, 3));
		assertEquals(90, earthFire.getHealthPoints());
		assertEquals(15, monster.attack(earthFire, 4));
		assertEquals(75, earthFire.getHealthPoints());

		assertEquals(6, monster.attack(earthEarth, 3));
		assertEquals(94, earthEarth.getHealthPoints());
		assertEquals(9, monster.attack(earthEarth, 4));
		assertEquals(85, earthEarth.getHealthPoints());

		assertEquals(10, monster.attack(airAir, 3));
		assertEquals(90, airAir.getHealthPoints());
		assertEquals(15, monster.attack(airAir, 4));
		assertEquals(75, airAir.getHealthPoints());

		assertEquals(8, monster.attack(airEarth, 3));
		assertEquals(92, airEarth.getHealthPoints());
		assertEquals(12, monster.attack(airEarth, 4));
		assertEquals(80, airEarth.getHealthPoints());

		assertEquals(12, monster.attack(airFire, 3));
		assertEquals(88, airFire.getHealthPoints());
		assertEquals(18, monster.attack(airFire, 4));
		assertEquals(70, airFire.getHealthPoints());

		assertEquals(10, monster.attack(airWater, 3));
		assertEquals(90, airWater.getHealthPoints());
		assertEquals(15, monster.attack(airWater, 4));
		assertEquals(75, airWater.getHealthPoints());
	}

	@Test
	public void AirAttacksAsSecondType() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new EarthType(), new AirType());

		Monster waterAir = new Monster(new WaterType(), new AirType());
		Monster waterFire = new Monster(new WaterType(), new FireType());
		Monster waterEarth = new Monster(new WaterType(), new EarthType());
		Monster waterWater = new Monster(new WaterType(), new WaterType());
		Monster fireAir = new Monster(new FireType(), new AirType());
		Monster fireWater = new Monster(new FireType(), new WaterType());
		Monster fireEarth = new Monster(new FireType(), new EarthType());
		Monster fireFire = new Monster(new FireType(), new FireType());
		Monster earthAir = new Monster(new EarthType(), new AirType());
		Monster earthWater = new Monster(new EarthType(), new WaterType());
		Monster earthFire = new Monster(new EarthType(), new FireType());
		Monster earthEarth = new Monster(new EarthType(), new EarthType());
		Monster airAir = new Monster(new AirType(), new AirType());
		Monster airEarth = new Monster(new AirType(), new EarthType());
		Monster airFire = new Monster(new AirType(), new FireType());
		Monster airWater = new Monster(new AirType(), new WaterType());
		Monster air = new Monster(new AirType());
		Monster fire = new Monster(new FireType());
		Monster earth = new Monster(new EarthType());
		Monster water = new Monster(new WaterType());
		
		monster.setSpecialAttacksLeft(20); //prevent NoSpecialAttacksLeftException

		assertEquals(8, monster.attack(air, 3));
		assertEquals(92, air.getHealthPoints());
		assertEquals(12, monster.attack(air, 4));
		assertEquals(80, air.getHealthPoints());
		
		assertEquals(10, monster.attack(fire, 3));
		assertEquals(90, fire.getHealthPoints());
		assertEquals(15, monster.attack(fire, 4));
		assertEquals(75, fire.getHealthPoints());
		
		assertEquals(12, monster.attack(earth, 3));
		assertEquals(88, earth.getHealthPoints());
		assertEquals(18, monster.attack(earth, 4));
		assertEquals(70, earth.getHealthPoints());
		
		assertEquals(10, monster.attack(water, 3));
		assertEquals(90, water.getHealthPoints());
		assertEquals(15, monster.attack(water, 4));
		assertEquals(75, water.getHealthPoints());

		assertEquals(8, monster.attack(waterAir, 3));
		assertEquals(92, waterAir.getHealthPoints());
		assertEquals(12, monster.attack(waterAir, 4));
		assertEquals(80, waterAir.getHealthPoints());

		assertEquals(10, monster.attack(waterFire, 3));
		assertEquals(90, waterFire.getHealthPoints());
		assertEquals(15, monster.attack(waterFire, 4));
		assertEquals(75, waterFire.getHealthPoints());

		assertEquals(12, monster.attack(waterEarth, 3));
		assertEquals(88, waterEarth.getHealthPoints());
		assertEquals(18, monster.attack(waterEarth, 4));
		assertEquals(70, waterEarth.getHealthPoints());

		assertEquals(10, monster.attack(waterWater, 3));
		assertEquals(90, waterWater.getHealthPoints());
		assertEquals(15, monster.attack(waterWater, 4));
		assertEquals(75, waterWater.getHealthPoints());

		assertEquals(8, monster.attack(fireAir, 3));
		assertEquals(92, fireAir.getHealthPoints());
		assertEquals(12, monster.attack(fireAir, 4));
		assertEquals(80, fireAir.getHealthPoints());

		assertEquals(10, monster.attack(fireWater, 3));
		assertEquals(90, fireWater.getHealthPoints());
		assertEquals(15, monster.attack(fireWater, 4));
		assertEquals(75, fireWater.getHealthPoints());

		assertEquals(12, monster.attack(fireEarth, 3));
		assertEquals(88, fireEarth.getHealthPoints());
		assertEquals(18, monster.attack(fireEarth, 4));
		assertEquals(70, fireEarth.getHealthPoints());

		assertEquals(10, monster.attack(fireFire, 3));
		assertEquals(90, fireFire.getHealthPoints());
		assertEquals(15, monster.attack(fireFire, 4));
		assertEquals(75, fireFire.getHealthPoints());

		assertEquals(10, monster.attack(earthAir, 3));
		assertEquals(90, earthAir.getHealthPoints());
		assertEquals(15, monster.attack(earthAir, 4));
		assertEquals(75, earthAir.getHealthPoints());

		assertEquals(12, monster.attack(earthWater, 3));
		assertEquals(88, earthWater.getHealthPoints());
		assertEquals(18, monster.attack(earthWater, 4));
		assertEquals(70, earthWater.getHealthPoints());

		assertEquals(12, monster.attack(earthFire, 3));
		assertEquals(88, earthFire.getHealthPoints());
		assertEquals(18, monster.attack(earthFire, 4));
		assertEquals(70, earthFire.getHealthPoints());

		assertEquals(14, monster.attack(earthEarth, 3));
		assertEquals(86, earthEarth.getHealthPoints());
		assertEquals(21, monster.attack(earthEarth, 4));
		assertEquals(65, earthEarth.getHealthPoints());

		assertEquals(6, monster.attack(airAir, 3));
		assertEquals(94, airAir.getHealthPoints());
		assertEquals(9, monster.attack(airAir, 4));
		assertEquals(85, airAir.getHealthPoints());

		assertEquals(10, monster.attack(airEarth, 3));
		assertEquals(90, airEarth.getHealthPoints());
		assertEquals(15, monster.attack(airEarth, 4));
		assertEquals(75, airEarth.getHealthPoints());

		assertEquals(8, monster.attack(airFire, 3));
		assertEquals(92, airFire.getHealthPoints());
		assertEquals(12, monster.attack(airFire, 4));
		assertEquals(80, airFire.getHealthPoints());

		assertEquals(8, monster.attack(airWater, 3));
		assertEquals(92, airWater.getHealthPoints());
		assertEquals(12, monster.attack(airWater, 4));
		assertEquals(80, airWater.getHealthPoints());
	}

	@Test
	public void EarthAttacksAsSecondType() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new EarthType(), new EarthType());

		Monster waterAir = new Monster(new WaterType(), new AirType());
		Monster waterFire = new Monster(new WaterType(), new FireType());
		Monster waterEarth = new Monster(new WaterType(), new EarthType());
		Monster waterWater = new Monster(new WaterType(), new WaterType());
		Monster fireAir = new Monster(new FireType(), new AirType());
		Monster fireWater = new Monster(new FireType(), new WaterType());
		Monster fireEarth = new Monster(new FireType(), new EarthType());
		Monster fireFire = new Monster(new FireType(), new FireType());
		Monster earthAir = new Monster(new EarthType(), new AirType());
		Monster earthWater = new Monster(new EarthType(), new WaterType());
		Monster earthFire = new Monster(new EarthType(), new FireType());
		Monster earthEarth = new Monster(new EarthType(), new EarthType());
		Monster airAir = new Monster(new AirType(), new AirType());
		Monster airEarth = new Monster(new AirType(), new EarthType());
		Monster airFire = new Monster(new AirType(), new FireType());
		Monster airWater = new Monster(new AirType(), new WaterType());
		Monster air = new Monster(new AirType());
		Monster fire = new Monster(new FireType());
		Monster earth = new Monster(new EarthType());
		Monster water = new Monster(new WaterType());
		
		monster.setSpecialAttacksLeft(20); //prevent NoSpecialAttacksLeftException

		assertEquals(10, monster.attack(air, 3));
		assertEquals(90, air.getHealthPoints());
		assertEquals(15, monster.attack(air, 4));
		assertEquals(75, air.getHealthPoints());
		
		assertEquals(8, monster.attack(fire, 3));
		assertEquals(92, fire.getHealthPoints());
		assertEquals(12, monster.attack(fire, 4));
		assertEquals(80, fire.getHealthPoints());
		
		assertEquals(10, monster.attack(earth, 3));
		assertEquals(90, earth.getHealthPoints());
		assertEquals(15, monster.attack(earth, 4));
		assertEquals(75, earth.getHealthPoints());
		
		assertEquals(12, monster.attack(water, 3));
		assertEquals(88, water.getHealthPoints());
		assertEquals(18, monster.attack(water, 4));
		assertEquals(70, water.getHealthPoints());

		assertEquals(12, monster.attack(waterAir, 3));
		assertEquals(88, waterAir.getHealthPoints());
		assertEquals(18, monster.attack(waterAir, 4));
		assertEquals(70, waterAir.getHealthPoints());

		assertEquals(10, monster.attack(waterFire, 3));
		assertEquals(90, waterFire.getHealthPoints());
		assertEquals(15, monster.attack(waterFire, 4));
		assertEquals(75, waterFire.getHealthPoints());

		assertEquals(12, monster.attack(waterEarth, 3));
		assertEquals(88, waterEarth.getHealthPoints());
		assertEquals(18, monster.attack(waterEarth, 4));
		assertEquals(70, waterEarth.getHealthPoints());

		assertEquals(14, monster.attack(waterWater, 3));
		assertEquals(86, waterWater.getHealthPoints());
		assertEquals(21, monster.attack(waterWater, 4));
		assertEquals(65, waterWater.getHealthPoints());

		assertEquals(8, monster.attack(fireAir, 3));
		assertEquals(92, fireAir.getHealthPoints());
		assertEquals(12, monster.attack(fireAir, 4));
		assertEquals(80, fireAir.getHealthPoints());

		assertEquals(10, monster.attack(fireWater, 3));
		assertEquals(90, fireWater.getHealthPoints());
		assertEquals(15, monster.attack(fireWater, 4));
		assertEquals(75, fireWater.getHealthPoints());

		assertEquals(8, monster.attack(fireEarth, 3));
		assertEquals(92, fireEarth.getHealthPoints());
		assertEquals(12, monster.attack(fireEarth, 4));
		assertEquals(80, fireEarth.getHealthPoints());

		assertEquals(6, monster.attack(fireFire, 3));
		assertEquals(94, fireFire.getHealthPoints());
		assertEquals(9, monster.attack(fireFire, 4));
		assertEquals(85, fireFire.getHealthPoints());

		assertEquals(10, monster.attack(earthAir, 3));
		assertEquals(90, earthAir.getHealthPoints());
		assertEquals(15, monster.attack(earthAir, 4));
		assertEquals(75, earthAir.getHealthPoints());

		assertEquals(12, monster.attack(earthWater, 3));
		assertEquals(88, earthWater.getHealthPoints());
		assertEquals(18, monster.attack(earthWater, 4));
		assertEquals(70, earthWater.getHealthPoints());

		assertEquals(8, monster.attack(earthFire, 3));
		assertEquals(92, earthFire.getHealthPoints());
		assertEquals(12, monster.attack(earthFire, 4));
		assertEquals(80, earthFire.getHealthPoints());

		assertEquals(10, monster.attack(earthEarth, 3));
		assertEquals(90, earthEarth.getHealthPoints());
		assertEquals(15, monster.attack(earthEarth, 4));
		assertEquals(75, earthEarth.getHealthPoints());

		assertEquals(10, monster.attack(airAir, 3));
		assertEquals(90, airAir.getHealthPoints());
		assertEquals(15, monster.attack(airAir, 4));
		assertEquals(75, airAir.getHealthPoints());

		assertEquals(10, monster.attack(airEarth, 3));
		assertEquals(90, airEarth.getHealthPoints());
		assertEquals(15, monster.attack(airEarth, 4));
		assertEquals(75, airEarth.getHealthPoints());

		assertEquals(8, monster.attack(airFire, 3));
		assertEquals(92, airFire.getHealthPoints());
		assertEquals(12, monster.attack(airFire, 4));
		assertEquals(80, airFire.getHealthPoints());

		assertEquals(12, monster.attack(airWater, 3));
		assertEquals(88, airWater.getHealthPoints());
		assertEquals(18, monster.attack(airWater, 4));
		assertEquals(70, airWater.getHealthPoints());
	}

	@Test(expected = NoSpecialAttacksLeftException.class)
	public void onlyFourSpecialAttacks() throws NoSpecialAttacksLeftException {
		Monster monster = new Monster(new WaterType());

		Monster enemyMonster = new Monster(new AirType());

		monster.attack(enemyMonster, 2);

		assertEquals(3, monster.getSpecialAttacksLeft());

		monster.attack(enemyMonster, 2);
		monster.attack(enemyMonster, 2);

		assertEquals(1, monster.getSpecialAttacksLeft());

		monster.attack(enemyMonster, 2);

		monster.attack(enemyMonster, 2); // throws exception at fifth call, this line is never executed, though
											// necessary to pass the test
	}
}
