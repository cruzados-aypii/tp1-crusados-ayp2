import org.junit.Test;

import logicFunctionality.*;

import static org.junit.Assert.*;

public class PlayerTests {

	@Test
	public void createNewPlayerAndGetName() {
		Player aNewPlayer = new Player("Un jugador");
		assertEquals("Un jugador", aNewPlayer.getName());
	}
	
	@Test
	public void setPlayerNameToLoser() {
		Player toBeLoser = new Player("Not a loser yet");
		toBeLoser.setName("Loser");
		assertEquals("Loser", toBeLoser.getName());
	}
	
	@Test
	public void setPlayerNameEmpty() {
		Player player1 = new Player("");
		Player player2 = new Player("George");
		Player player3 = new Player(null);
		Player player4 = new Player("Jorge");
		
		player2.setName("");
		player4.setName(null);
		
		assertEquals("Player 1", player1.getName());
		assertEquals("Player 2", player2.getName());
		assertEquals("Player 3", player3.getName());
		assertEquals("Player 4", player4.getName());
	}
	
	@Test
	public void resetPlayerCounter() {
		new Player("a");
		new Player("b");
		
		assertEquals(2, Player.getNumberOfPlayers());
		
		Player.resetNumberOfPlayers();
		
		assertEquals(0, Player.getNumberOfPlayers());
	}
}
