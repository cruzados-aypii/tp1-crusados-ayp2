package logicFunctionality;

public class GameOverException extends Exception {

	private static final long serialVersionUID = -2473426225875943658L;

	public GameOverException() {
		super("The game is over, restart the game.");
	}
}
