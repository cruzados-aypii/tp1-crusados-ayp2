package logicFunctionality;

public class FireType implements MonsterType {

	@Override
	public int calculateDamage(MonsterTypeVisitor enemyType, boolean isSpecialAttack) {
		return enemyType.defend(this, isSpecialAttack);
	}

	@Override
	public MonsterTypeVisitor createVisitor() {
		return new FireTypeVisitor();
	}

	public String toString() {
		return "fire";
	}
}
