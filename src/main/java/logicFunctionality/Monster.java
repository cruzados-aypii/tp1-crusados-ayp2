package logicFunctionality;

public class Monster {

	private MonsterType typeX;
	private MonsterType typeY;
	private int healthPoints;
	private int specialAttacksLeft;

	/**
	 * Creates a Monster starting at 100 health points, 4 special attacks and two
	 * types. The Monster's type can be Fire, Air, Water or Earth, following this
	 * set of weaknesses and resistances: - Fire deals increased damage versus
	 * AirType and resists EarthType. - Water deals increased damage versus FireType
	 * and resists FireType. - Air deals increased damage versus EarthType and
	 * resists AirType. - Earth deals increased damage versus WaterType and resists
	 * WaterType.
	 * 
	 * If the same type is chosen twice, the Monster will have that type's
	 * resistance and weakness doubled. Doesn't affect the Monster's attack power.
	 * 
	 * @param typeX
	 * @param typeY
	 */
	public Monster(MonsterType typeX, MonsterType typeY) {
		this.setTypeX(typeX);
		this.setTypeY(typeY);
		this.setHealthPoints(100);
		this.setSpecialAttacksLeft(4);
	}

	/**
	 * Creates a Monster starting at 100 health points, 4 special attacks and a
	 * single type. The Monster's type can be Fire, Air, Water or Earth, following
	 * this set of weaknesses and resistances:
	 * - Fire deals increased damage versus AirType and resists EarthType.
	 * - Water deals increased damage versus FireType and resists FireType.
	 * - Air deals increased damage versus EarthType and resists AirType.
	 * - Earth deals increased damage versus WaterType and resists WaterType.
	 * 
	 * @param typeX
	 * @param typeY
	 */
	public Monster(MonsterType type) {
		this.setTypeX(type);
		this.setTypeY(null);
		this.setHealthPoints(100);
		this.setSpecialAttacksLeft(4);
	}

	/**
	 * Attacks the specified enemyMonster using the indicated typeOfAttack:
	 * - 1 TypeX normal attack
	 * - 2 TypeX special attack
	 * - 3 TypeY normal attack
	 * - 4 TypeY special attack
	 * 
	 * Special attacks can only be used up to 4 times in the same
	 * battle. Returns the amount of damage dealt to the enemy monster.
	 * 
	 * @param enemyMonster
	 *            the Monster being attacked
	 * @param typeOfAttack
	 *            the attack chosen by the Player
	 * @return damageDealt the damage dealt to the enemy Monster
	 */
	public int attack(Monster enemyMonster, int typeOfAttack) throws NoSpecialAttacksLeftException {
		int damageDealt = 0;
		MonsterTypeVisitor enemyTypeX = enemyMonster.getTypeX().createVisitor();
		MonsterTypeVisitor enemyTypeY = ((enemyMonster.getTypeY() != null) ? enemyMonster.getTypeY().createVisitor()
				: null);

		switch (typeOfAttack) {
		case 1:
			damageDealt = this.normalAttack(this.typeX, enemyTypeX, enemyTypeY);
			break;

		case 2:
			damageDealt = specialAttack(this.typeX, enemyTypeX, enemyTypeY);
			break;

		case 3:
			damageDealt = this.normalAttack(this.typeY, enemyTypeX, enemyTypeY);
			break;

		case 4:
			damageDealt = specialAttack(this.typeY, enemyTypeX, enemyTypeY);
			break;
		}

		if (damageDealt <= enemyMonster.getHealthPoints()) {
			enemyMonster.setHealthPoints(enemyMonster.getHealthPoints() - damageDealt);
		} else {
			damageDealt = enemyMonster.getHealthPoints();
			enemyMonster.setHealthPoints(0);
		}
		return damageDealt;
	}

	/**
	 * Inner method that calculates the damage of a regular attack.
	 * 
	 * @param alliedType
	 * 				The Monster's type used for the attack
	 * @param enemyTypeX
	 * 				The first type of the enemy Monster
	 * @param enemyTypeY
	 * 				The second type of the enemy Monster, might be null
	 * @return
	 */
	private int normalAttack(MonsterType alliedType, MonsterTypeVisitor enemyTypeX, MonsterTypeVisitor enemyTypeY) {

		return alliedType.calculateDamage(enemyTypeX, false)
				+ ((enemyTypeY != null) ? alliedType.calculateDamage(enemyTypeY, false) - 10 : 0);
	}

	/**
	 * Inner method that calculates the damage of a special attack.
	 * 
	 * @param alliedType
	 * 				The Monster's type used for the attack
	 * @param enemyTypeX
	 * 				The first type of the enemy Monster
	 * @param enemyTypeY
	 * 				The second type of the enemy Monster, might be null
	 * @return
	 * @throws NoSpecialAttacksLeftException
	 */
	private int specialAttack(MonsterType alliedType, MonsterTypeVisitor enemyTypeX, MonsterTypeVisitor enemyTypeY)
			throws NoSpecialAttacksLeftException {

		if (getSpecialAttacksLeft() > 0) {

			setSpecialAttacksLeft(getSpecialAttacksLeft() - 1);
			return alliedType.calculateDamage(enemyTypeX, true)
					+ ((enemyTypeY != null) ? alliedType.calculateDamage(enemyTypeY, true) - 15 : 0);

		} else {
			throw new NoSpecialAttacksLeftException();
		}
	}

	// Getters and setters

	public MonsterType getTypeX() {
		return typeX;
	}

	public void setTypeX(MonsterType typeX) {
		this.typeX = typeX;
	}

	public MonsterType getTypeY() {
		return typeY;
	}

	public void setTypeY(MonsterType typeY) {
		this.typeY = typeY;
	}

	public int getHealthPoints() {
		return healthPoints;
	}

	public void setHealthPoints(int healthPoints) {
		this.healthPoints = healthPoints;
	}

	public int getSpecialAttacksLeft() {
		return specialAttacksLeft;
	}

	public void setSpecialAttacksLeft(int specialAttacksLeft) {
		this.specialAttacksLeft = specialAttacksLeft;
	}
}
