package logicFunctionality;

public interface MonsterTypeVisitor {

	public abstract int defend(WaterType attackingType, boolean isSpecialAttack);
	
	public abstract int defend(FireType attackingType, boolean isSpecialAttack);
	
	public abstract int defend(AirType attackingType, boolean isSpecialAttack);
	
	public abstract int defend(EarthType attackingType, boolean isSpecialAttack);
}
