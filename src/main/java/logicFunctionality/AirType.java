package logicFunctionality;

public class AirType implements MonsterType {

	@Override
	public int calculateDamage(MonsterTypeVisitor enemyType, boolean isSpecialAttack) {
		return enemyType.defend(this, isSpecialAttack);
	}

	@Override
	public MonsterTypeVisitor createVisitor() {
		return new AirTypeVisitor();
	}

	public String toString() {
		return "air";
	}
}
