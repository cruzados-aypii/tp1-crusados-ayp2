package logicFunctionality;

public class NoSpecialAttacksLeftException extends Exception {

	private static final long serialVersionUID = -3087064862430505534L;

	public NoSpecialAttacksLeftException() {
		super("There are no special attacks left.");
	}
}
