package logicFunctionality;

public class EarthType implements MonsterType {

	@Override
	public int calculateDamage(MonsterTypeVisitor enemyType, boolean isSpecialAttack) {
		return enemyType.defend(this, isSpecialAttack);
	}

	@Override
	public MonsterTypeVisitor createVisitor() {
		return new EarthTypeVisitor();
	}

	public String toString() {
		return "earth";
	}
}
