package logicFunctionality;

public class WaterType implements MonsterType {

	@Override
	public int calculateDamage(MonsterTypeVisitor enemyType, boolean isSpecialAttack) {
		return enemyType.defend(this, isSpecialAttack);
	}

	@Override
	public MonsterTypeVisitor createVisitor() {
		return new WaterTypeVisitor();
	}

	public String toString() {
		return "water";
	}
}
