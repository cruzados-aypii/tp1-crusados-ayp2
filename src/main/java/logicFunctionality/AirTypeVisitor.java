package logicFunctionality;

public class AirTypeVisitor implements MonsterTypeVisitor {

	@Override
	/**
	 * Returns damage received when defending from a WaterType Monster.
	 * isSpecialAttack specifies whether a special attack is used or not. Air takes
	 * regular damage from Water attacks.
	 */
	public int defend(WaterType attackingType, boolean isSpecialAttack) {
		return (isSpecialAttack) ? 15 : 10;
	}

	@Override
	/**
	 * Returns damage received when defending from a FireType Monster.
	 * isSpecialAttack specifies whether a special attack is used or not. Air takes
	 * increased damage from Fire attacks.
	 */
	public int defend(FireType attackingType, boolean isSpecialAttack) {
		return (int) ((isSpecialAttack) ? 15*1.2 : 10*1.2);
	}

	@Override
	/**
	 * Returns damage received when defending from an AirType Monster.
	 * isSpecialAttack specifies whether a special attack is used or not. Air takes
	 * reduced damage from Air attacks.
	 */
	public int defend(AirType attackingType, boolean isSpecialAttack) {
		return (int) ((isSpecialAttack) ? 15*0.8 : 10*0.8);
	}

	@Override
	/**
	 * Returns damage dealt when defending from an EarthType Monster.
	 * isSpecialAttack specifies whether a special attack is used or not. Air takes
	 * regular damage from Earth attacks.
	 */
	public int defend(EarthType attackingType, boolean isSpecialAttack) {
		return (isSpecialAttack) ? 15 : 10;
	}
}
