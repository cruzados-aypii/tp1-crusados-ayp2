package logicFunctionality;

public interface MonsterType {

	public abstract int calculateDamage(MonsterTypeVisitor enemyType, boolean isSpecialAttack);
	
	public abstract MonsterTypeVisitor createVisitor();
}
