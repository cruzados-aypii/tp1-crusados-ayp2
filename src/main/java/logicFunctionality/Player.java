package logicFunctionality;

public class Player {

	private String playerName;
	private Monster playerMonster;
	private static int numberOfPlayers = 0;
	private int playerNumber;

	public Player(String name) {
		numberOfPlayers++;
		this.playerName = name;
		this.playerMonster = null;
		this.playerNumber = numberOfPlayers;

		ifEmptyName();
	}

	public String getName() {
		return playerName;
	}

	public void setName(String newName) {
		this.playerName = newName;
		ifEmptyName();
	}

	public Monster getPlayerMonster() {
		return playerMonster;
	}

	public void setPlayerMonster(Monster newMonster) {
		this.playerMonster = newMonster;
	}

	public static int getNumberOfPlayers() {
		return numberOfPlayers;
	}

	/**
	 * This method was implemented so all JUnit test cases could pass when run together.
	 */
	public static void resetNumberOfPlayers() {
		numberOfPlayers = 0;
	}

	private void ifEmptyName() {
		if (playerName == null || playerName.equals("")) {
			playerName = "Player " + playerNumber;
		}
	}
}
