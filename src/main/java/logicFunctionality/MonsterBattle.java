package logicFunctionality;

public class MonsterBattle {

	private Player player1;
	private Player player2;
	private boolean player1Turn;
	private boolean gameOver;

	public MonsterBattle(String player1Name, String player2Name) {
		this.player1 = new Player(player1Name);
		this.player2 = new Player(player2Name);
		this.gameOver = false;
		this.player1Turn = true;
	}

	public void setPlayer1Monster(MonsterType player1typeX, MonsterType player1typeY) {
		this.player1.setPlayerMonster(new Monster(player1typeX, player1typeY));
	}

	public void setPlayer2Monster(MonsterType player2typeX, MonsterType player2typeY) {
		this.player2.setPlayerMonster(new Monster(player2typeX, player2typeY));
	}

	public int playerAttacks(int typeOfAttack) throws NoSpecialAttacksLeftException, GameOverException {
		int damageDealt = 0;
		Monster player1Monster = player1.getPlayerMonster();
		Monster player2Monster = player2.getPlayerMonster();

		if (player1Turn && !gameOver) {
			damageDealt = player1Monster.attack(player2Monster, typeOfAttack);
			gameOver = player2Monster.getHealthPoints() == 0;
		} else if (!gameOver) {
			damageDealt = player2Monster.attack(player1Monster, typeOfAttack);
			gameOver = player1Monster.getHealthPoints() == 0;
		} else {
			throw new GameOverException();
		}

		player1Turn = !player1Turn;

		return damageDealt;
	}

	public boolean isOver() {
		return gameOver;
	}

	public String getWinner() {
		String winner = "";

		if (gameOver && player1Turn) {
			winner = player2.getName();

		} else if (gameOver) {
			winner = player1.getName();
		}

		return winner;
	}

	public String createSummary(int damageDealt) {
		String player1Types = " (" + player1.getPlayerMonster().getTypeX().toString()
				+ ((player1.getPlayerMonster().getTypeY() != null)
						? ", " + player1.getPlayerMonster().getTypeY().toString() + ")"
						: ")");
		
		String player2Types = " (" + player2.getPlayerMonster().getTypeX().toString()
				+ ((player2.getPlayerMonster().getTypeY() != null)
						? ", " + player2.getPlayerMonster().getTypeY().toString() + ")"
						: ")");

		return "Damage dealt: " + damageDealt

				+ "\n\n" + player1.getName() + player1Types + "\nHealth points left: " + player1.getPlayerMonster().getHealthPoints()
				+ "\nSpecial attacks left: " + player1.getPlayerMonster().getSpecialAttacksLeft()

				+ "\n\n" + player2.getName() + player2Types +  "\nHealth points left: " + player2.getPlayerMonster().getHealthPoints()
				+ "\nSpecial attacks left: " + player2.getPlayerMonster().getSpecialAttacksLeft();
	}

	// Getters & setters
	public Player getPlayer1() {
		return player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer1(Player newPlayer1) {
		player1 = newPlayer1;
	}

	public void setPlayer2(Player newPlayer2) {
		player2 = newPlayer2;
	}

	public boolean getPlayer1Turn() {
		return player1Turn;
	}
}
