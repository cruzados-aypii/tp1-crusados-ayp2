package graphicMenu;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logicFunctionality.*;

public class Board {

	private Stage currentStage;

	private GridPane grid;

	private MonsterBattle game;

	private Label summary;
	private Label currentPlayer;

	private Button p1attack1;
	private Button p1attack2;
	private Button p1attack3;
	private Button p1attack4;

	private Button p2attack1;
	private Button p2attack2;
	private Button p2attack3;
	private Button p2attack4;

	private boolean player1Turn;
	private boolean player1SingleType;
	private boolean player2SingleType;
	private boolean player1HasSpecialAttacks;
	private boolean player2HasSpecialAttacks;

	public Board(MonsterBattle game, boolean player1SingleType, boolean player2SingleType) {
		this.game = game;
		this.player1Turn = game.getPlayer1Turn();
		this.player1SingleType = player1SingleType;
		this.player2SingleType = player2SingleType;
		this.player1HasSpecialAttacks = game.getPlayer1().getPlayerMonster().getSpecialAttacksLeft() > 0;
		this.player2HasSpecialAttacks = game.getPlayer2().getPlayerMonster().getSpecialAttacksLeft() > 0;

		createGrid();
	}

	/**
	 * Creates the grid which will contain all of the windows' elements.
	 */
	private void createGrid() {
		this.grid = new GridPane();

		createControlsPlayer1();
		createControlsPlayer2();

		summary = new Label();
		String player1Types = "(" + game.getPlayer1().getPlayerMonster().getTypeX().toString()
				+ ((player1SingleType) ? ")"
						: (", " + game.getPlayer1().getPlayerMonster().getTypeY().toString() + ")"));

		String player2Types = "(" + game.getPlayer2().getPlayerMonster().getTypeX().toString()
				+ ((player2SingleType) ? ")"
						: (", " + game.getPlayer2().getPlayerMonster().getTypeY().toString() + ")"));

		summary.setText("Game start\n\n" + game.getPlayer1().getName() + " " + player1Types
				+ "\nHealth points: 100\nSpecial attacks left: 4\n\n" + game.getPlayer2().getName() + " " + player2Types
				+ "\nHealth points: 100\nSpecial attacks left: 4");
		summary.setId("summary");
		grid.add(summary, 0, 1, 3, 8);

		currentPlayer = new Label();
		currentPlayer.setText(game.getPlayer1().getName() + "'s turn");
		currentPlayer.setId("current-player");
		grid.add(currentPlayer, 0, 0, 6, 1);

		grid.add(p1attack1, 10, 4, 2, 1);
		grid.add(p1attack2, 10, 5, 2, 1);

		Insets buttonInset = new Insets(5, 0, 5, 140);

		GridPane.setHalignment(p1attack1, HPos.RIGHT);
		GridPane.setHalignment(p1attack2, HPos.RIGHT);
		GridPane.setMargin(p1attack1, buttonInset);
		GridPane.setMargin(p1attack2, buttonInset);

		GridPane.setHalignment(p2attack1, HPos.RIGHT);
		GridPane.setHalignment(p2attack2, HPos.RIGHT);
		GridPane.setMargin(p2attack1, buttonInset);
		GridPane.setMargin(p2attack2, buttonInset);

		GridPane.setValignment(p1attack1, VPos.BOTTOM);
		GridPane.setValignment(p1attack2, VPos.BOTTOM);

		GridPane.setValignment(p2attack1, VPos.BOTTOM);
		GridPane.setValignment(p2attack2, VPos.BOTTOM);

		GridPane.setHalignment(summary, HPos.LEFT);
		GridPane.setValignment(summary, VPos.CENTER);
		GridPane.setMargin(summary, new Insets(0, 0, 0, 15));

		GridPane.setHalignment(currentPlayer, HPos.CENTER);
		GridPane.setValignment(currentPlayer, VPos.TOP);
		GridPane.setMargin(currentPlayer, new Insets(10, 0, 130, 0));

		if (!player1SingleType && !player1DoubleType()) {
			grid.add(p1attack3, 10, 6, 2, 1);
			grid.add(p1attack4, 10, 7, 2, 1);

			GridPane.setHalignment(p1attack3, HPos.CENTER);
			GridPane.setHalignment(p1attack4, HPos.CENTER);
			GridPane.setValignment(p1attack3, VPos.BOTTOM);
			GridPane.setValignment(p1attack4, VPos.BOTTOM);
			GridPane.setMargin(p1attack3, buttonInset);
			GridPane.setMargin(p1attack4, buttonInset);
		}

		if (!player2SingleType && !player2DoubleType()) {
			GridPane.setHalignment(p2attack3, HPos.CENTER);
			GridPane.setHalignment(p2attack4, HPos.CENTER);
			GridPane.setValignment(p2attack3, VPos.BOTTOM);
			GridPane.setValignment(p2attack4, VPos.BOTTOM);
			GridPane.setMargin(p2attack3, buttonInset);
			GridPane.setMargin(p2attack4, buttonInset);
		}
	}

	/**
	 * Creates the controls player 1 will interact with.
	 */
	private void createControlsPlayer1() {

		p1attack1 = new Button("Normal " + game.getPlayer1().getPlayerMonster().getTypeX().toString() + " attack");
		p1attack2 = new Button("Special " + game.getPlayer1().getPlayerMonster().getTypeX().toString() + " attack");
		p1attack1.setOnAction(new AttackOrder(this, 1));
		p1attack2.setOnAction(new AttackOrder(this, 2));

		if (!player1SingleType && !player1DoubleType()) {

			p1attack3 = new Button("Normal " + game.getPlayer1().getPlayerMonster().getTypeY().toString() + " attack");
			p1attack4 = new Button("Special " + game.getPlayer1().getPlayerMonster().getTypeY().toString() + " attack");
			p1attack3.setOnAction(new AttackOrder(this, 3));
			p1attack4.setOnAction(new AttackOrder(this, 4));
		}
	}

	/**
	 * Creates the controls player 2 will interact with.
	 */
	private void createControlsPlayer2() {

		p2attack1 = new Button("Normal " + game.getPlayer2().getPlayerMonster().getTypeX().toString() + " attack");
		p2attack2 = new Button("Special " + game.getPlayer2().getPlayerMonster().getTypeX().toString() + " attack");
		p2attack1.setOnAction(new AttackOrder(this, 1));
		p2attack2.setOnAction(new AttackOrder(this, 2));

		if (!player2SingleType && !player2DoubleType()) {

			p2attack3 = new Button("Normal " + game.getPlayer2().getPlayerMonster().getTypeY().toString() + " attack");
			p2attack4 = new Button("Special " + game.getPlayer2().getPlayerMonster().getTypeY().toString() + " attack");
			p2attack3.setOnAction(new AttackOrder(this, 3));
			p2attack4.setOnAction(new AttackOrder(this, 4));
		}
	}

	/**
	 * Makes the window shift between the controls for player 1 and player 2.
	 */
	public void mutateGrid() {

		if (player1Turn) {
			grid.getChildren().remove(p2attack1);
			if (grid.getChildren().contains(p2attack2)) {
				grid.getChildren().remove(p2attack2);
			}

			if (!player2SingleType && !player2DoubleType()) {
				grid.getChildren().remove(p2attack3);
				if (grid.getChildren().contains(p2attack4)) {
					grid.getChildren().remove(p2attack4);
				}
			}

			grid.add(p1attack1, 10, 4, 2, 1);
			if (player1HasSpecialAttacks) {
				grid.add(p1attack2, 10, 5, 2, 1);
			}

			if (!player1SingleType && !player1DoubleType()) {
				grid.add(p1attack3, 10, 6, 2, 1);
				if (player1HasSpecialAttacks) {
					grid.add(p1attack4, 10, 7, 2, 1);
				}
			}

			currentPlayer.setText(game.getPlayer1().getName() + "'s turn");

		} else {
			grid.getChildren().remove(p1attack1);
			if (grid.getChildren().contains(p1attack2)) {
				grid.getChildren().remove(p1attack2);
			}

			if (!player1SingleType && !player1DoubleType()) {
				grid.getChildren().remove(p1attack3);
				if (grid.getChildren().contains(p1attack4)) {
					grid.getChildren().remove(p1attack4);
				}
			}

			grid.add(p2attack1, 10, 4, 2, 1);
			if (player2HasSpecialAttacks) {
				grid.add(p2attack2, 10, 5, 2, 1);
			}

			if (!player2SingleType && !player2DoubleType()) {
				grid.add(p2attack3, 10, 6, 2, 1);
				if (player2HasSpecialAttacks) {
					grid.add(p2attack4, 10, 7, 2, 1);
				}
			}

			currentPlayer.setText(game.getPlayer2().getName() + "'s turn");
		}

	}

	public void show(Stage stage) {
		this.currentStage = stage;

		Scene scene = new Scene(grid, 1100, 550);
		scene.getStylesheets().add(getClass().getResource("/resources/Board.css").toExternalForm());
		currentStage.setScene(scene);
		currentStage.setTitle("Monster Battle");
		currentStage.getIcons().add(new Image(getClass().getResource("/resources/Icon-Storn.jpg").toExternalForm()));
		currentStage.setResizable(false);
		currentStage.centerOnScreen();
		currentStage.show();
	}

	/**
	 * Displays the results of the game.
	 */
	public void showResult() {

		MediaPlayer victory = new MediaPlayer(
				new Media(getClass().getResource("/resources/Chrono_Cross_OST_-_Victory.mp3").toExternalForm()));
		victory.play();

		Stage dialog = new Stage();

		BorderPane winnerPane = new BorderPane();
		winnerPane.setPadding(new Insets(10.0));
		Text result;
		Button restart = new Button("New game");

		result = new Text("Player " + game.getWinner() + " won");
		restart.setOnAction(new RestartGame(this.currentStage));

		result.setId("result");
		winnerPane.setCenter(result);
		winnerPane.setBottom(restart);

		Scene winnerScene = new Scene(winnerPane);
		winnerPane.getStylesheets().add(getClass().getResource("/resources/WinnerPane.css").toExternalForm());

		dialog.setScene(winnerScene);
		dialog.initOwner(currentStage);
		dialog.initModality(Modality.WINDOW_MODAL);
		dialog.setResizable(false);
		dialog.getIcons().add(new Image(getClass().getResource("/resources/Icon-Storn.jpg").toExternalForm()));

		dialog.showAndWait();
	}

	public void processSpecialAttacks() {
		this.player1HasSpecialAttacks = game.getPlayer1().getPlayerMonster().getSpecialAttacksLeft() > 0;
		this.player2HasSpecialAttacks = game.getPlayer2().getPlayerMonster().getSpecialAttacksLeft() > 0;
	}

	private boolean player1DoubleType() {
		if (!player1SingleType) {
			return game.getPlayer1().getPlayerMonster().getTypeX().toString()
					.equals(game.getPlayer1().getPlayerMonster().getTypeY().toString());
		} else
			return false;
	}

	private boolean player2DoubleType() {
		if (!player2SingleType) {
			return game.getPlayer2().getPlayerMonster().getTypeX().toString()
					.equals(game.getPlayer2().getPlayerMonster().getTypeY().toString());
		} else
			return false;
	}

	// Getters and setters
	public Label getSummary() {
		return summary;
	}

	public void setPlayer1Turn(boolean turn) {
		player1Turn = turn;
	}

	public MonsterBattle getGame() {
		return game;
	}

	public Label getCurrentPlayer() {
		return currentPlayer;
	}
}
