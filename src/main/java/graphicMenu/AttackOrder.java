package graphicMenu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import logicFunctionality.GameOverException;
import logicFunctionality.MonsterBattle;
import logicFunctionality.NoSpecialAttacksLeftException;

public class AttackOrder implements EventHandler<ActionEvent> {

	private Board board;
	private MonsterBattle game;
	private int typeOfAttack;

	public AttackOrder(Board board, int typeOfAttack) {
		this.board = board;
		game = board.getGame();
		this.typeOfAttack = typeOfAttack;
	}

	@Override
	public void handle(ActionEvent event) {
		if (!game.isOver()) {
			try {
				MediaPlayer buttonClick = new MediaPlayer(new Media(getClass().getResource("/resources/button-30.mp3").toExternalForm()));
				buttonClick.play();
				
				board.getSummary().setText(game.createSummary(game.playerAttacks(typeOfAttack)));
				board.setPlayer1Turn(game.getPlayer1Turn());
				board.processSpecialAttacks();
				board.mutateGrid();

			} catch (NoSpecialAttacksLeftException e) {
				if (!board.getSummary().getText()
						.endsWith("\n\nNo special attacks left, please choose a different attack")) {
					board.getSummary().setText(
							board.getSummary().getText() + "\n\nNo special attacks left, please choose a different attack");
				}

			} catch (GameOverException e) {
				board.getSummary().setText(
						game.createSummary(0) + "\n\nGame is over, start a new game\nif you wish to keep playing");
			}

			if (game.isOver()) {
				board.showResult();
				board.getCurrentPlayer().setText(game.getWinner() + " is the winner");
			}

		} else {
			board.showResult();
		}
	}

}
