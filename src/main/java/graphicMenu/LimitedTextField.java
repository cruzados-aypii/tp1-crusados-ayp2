package graphicMenu;

import javafx.scene.control.TextField;

public class LimitedTextField extends TextField {

	private int limit;

	public LimitedTextField(String description, int limit) {
		super(description);
		this.limit = limit;
	}

	@Override
	public void replaceText(int start, int end, String text) {
		if (text.equals("")) {
			super.replaceText(start, end, text);
			
		} else if (getText().length() < limit) {
			super.replaceText(start, end, text);
		}
	}

	@Override
	public void replaceSelection(String text) {
		if (text.equals("")) {
			super.replaceSelection(text);
			
		} else if (getText().length() < limit) {
			
			if (text.length() > limit - getText().length()) {
				text = text.substring(0, limit - getText().length());
			}
			
			super.replaceSelection(text);
		}
	}
}
