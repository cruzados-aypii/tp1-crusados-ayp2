package graphicMenu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class StartGame implements EventHandler<ActionEvent> {

	private MonsterApplication aplicacion;
	
	public StartGame(MonsterApplication aplicacion) {

		this.aplicacion = aplicacion;
	}

	@Override
	public void handle(ActionEvent event) {
		MediaPlayer buttonClick = new MediaPlayer(new Media(getClass().getResource("/resources/button-30.mp3").toExternalForm()));
		buttonClick.play();
		
		aplicacion.startUp();
	}

}
