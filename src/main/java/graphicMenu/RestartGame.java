package graphicMenu;

import javafx.event.EventHandler;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import logicFunctionality.Player;
import javafx.event.ActionEvent;

public class RestartGame implements EventHandler<ActionEvent> {

	Stage boardStage;

	public RestartGame(Stage boardStage) {
		this.boardStage = boardStage;
	}

	@Override
	public void handle(ActionEvent event) {
		MediaPlayer buttonClick = new MediaPlayer(new Media(getClass().getResource("/resources/button-30.mp3").toExternalForm()));
		buttonClick.play();
		
		MonsterApplication newGame = new MonsterApplication();
		newGame.start(new Stage());
		Player.resetNumberOfPlayers();
		boardStage.close();
	}

}
