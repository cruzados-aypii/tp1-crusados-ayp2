package graphicMenu;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import logicFunctionality.MonsterBattle;

/**
 * Monster Battle application. Game starts from here.
 * 
 */
public class MonsterApplication extends Application {

	public static final String TITLE = "Monster Battle";

	private Stage currentStage;

	private GridPane grid;

	private LimitedTextField player1TextField;
	private LimitedTextField player2TextField;

	private Button helpButton;
	private Button startButton;

	@Override
	public void start(Stage stage) {

		currentStage = stage;

		createGrid();

		Scene escena = new Scene(grid, 400, 300);

		if (getClass().getResource("/resources/MonsterApplication.css") != null) {
			escena.getStylesheets().add(getClass().getResource("/resources/MonsterApplication.css").toExternalForm());
		} else {
			escena.getStylesheets().add(getClass().getResource("/MonsterApplication.css").toExternalForm());
		}
		stage.setScene(escena);
		stage.getIcons().add(new Image(getClass().getResource("/resources/Icon-Storn.jpg").toExternalForm()));
		stage.setTitle(TITLE);
		stage.setResizable(false);
		stage.centerOnScreen();
		stage.show();
	}

	/**
	 * Creates the grid that will contain all of the application window's elements.
	 */
	private void createGrid() {

		grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(20);
		grid.setVgap(20);

		Text textoTitulo = new Text(TITLE);
		textoTitulo.setId("text-title");

		createControls();

		grid.add(textoTitulo, 0, 0, 2, 1);
		grid.add(new Label("Player 1"), 0, 1);
		grid.add(player1TextField, 1, 1);
		grid.add(new Label("Player 2"), 0, 2);
		grid.add(player2TextField, 1, 2);
		grid.add(startButton, 0, 6, 2, 1);
		grid.add(helpButton, 0, 4, 2, 1);

		GridPane.setHalignment(helpButton, HPos.CENTER);
		GridPane.setHalignment(startButton, HPos.CENTER);
		GridPane.setHalignment(textoTitulo, HPos.CENTER);
	}

	/**
	 * Creates the elements the user will interact with.
	 */
	private void createControls() {

		player1TextField = new LimitedTextField("player 1's name", 15);
		player2TextField = new LimitedTextField("player 2's name", 15);

		startButton = new Button("Start");
		startButton.setOnAction(new StartGame(this));
		helpButton = new Button("Help");
		helpButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				MediaPlayer buttonClick = new MediaPlayer(
						new Media(getClass().getResource("/resources/button-30.mp3").toExternalForm()));
				buttonClick.play();

				Stage helpStage = new Stage();
				BorderPane helpWindow = new BorderPane();
				BorderPane innerBody = new BorderPane();
				Text top = new Text("Monster Battle");

				Text topInnerBody = new Text(
						"In this game, two players get to create custom monsters and make them fight. Each monster"
								+ " can have up to two types,\nand these are:");

				Text lowerInnerBody = new Text(
						"Water, weak to Earth and resistant to Fire.\nFire, weak to Water and resistant to Earth.\n"
								+ "Air, weak to Fire and resistant to Air.\nEarth, weak to Air and resistant to Water.");

				Text bottom = new Text("\nIt is possible to choose the same type twice,"
						+ " doubling that type's resistance and weakness, or choosing no second type.\nEach monster has two different attacks per type: a normal"
						+ " attack that deals 10 damage, and a special attack which\ndeals 15 damage. Special attacks can only be used four times in the same game,"
						+ " and both types share this counter. Depending on\nthe monster's resistances and weakness, it will take 20% more damage from a type its weak to,"
						+ " regular damage or 20% less if its\nresistant to the enemy's attack. Weaknesses and resistances stack with each other, meaning that a monster"
						+ " can be weak\nand resistant at the same time to a certain type. Both monsters start with 100 health points, and once of them reaches 0, the"
						+ "\nmonster faints and the opposing player wins the game.");

				top.setId("title-text");
				topInnerBody.setId("top-text");
				lowerInnerBody.setId("type-list-text");
				bottom.setId("bottom-text");

				innerBody.setTop(topInnerBody);
				innerBody.setBottom(lowerInnerBody);
				helpWindow.setTop(top);
				helpWindow.setCenter(innerBody);
				helpWindow.setBottom(bottom);

				Scene scene = new Scene(helpWindow);
				scene.getStylesheets().add(getClass().getResource("/resources/HelpWindow.css").toExternalForm());
				helpStage.setScene(scene);
				helpStage.setTitle("Game description");
				helpStage.getIcons()
						.add(new Image(getClass().getResource("/resources/Icon-Storn.jpg").toExternalForm()));
				helpStage.setResizable(false);
				helpStage.centerOnScreen();
				helpStage.showAndWait();
			}
		});
	}

	/**
	 * Creates a MonsterBattle game with the player names and passes it to the next
	 * window for Monster creation.
	 */
	public void startUp() {

		String player1Name = player1TextField.getText();
		String player2Name = player2TextField.getText();

		MonsterBattle game = new MonsterBattle(player1Name, player2Name);

		MonsterCreationWindow creationWindow = new MonsterCreationWindow(game);
		creationWindow.show(new Stage(), game.getPlayer1().getName());

		currentStage.close();
	}

	public static void main(String[] args) {

		launch(args);
	}
}
