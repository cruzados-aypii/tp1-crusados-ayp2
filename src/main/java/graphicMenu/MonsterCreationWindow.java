package graphicMenu;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import logicFunctionality.*;

public class MonsterCreationWindow {

	private Stage currentStage;
	
	private GridPane grid;

	private Label firstType;
	private Label secondType;
	private ChoiceBox<Object> typeXOptions;
	private ChoiceBox<Object> typeYOptions;
	private Button done;

	private final MonsterType[] types = { new WaterType(), new FireType(), new AirType(), new EarthType(), null };

	private MonsterType typeX;
	private MonsterType typeY;
	private MonsterBattle game;

	public MonsterCreationWindow(MonsterBattle game) {
		this.game = game;
		createGrid();

	}

	/**
	 * Creates the grid that will contain the window's elements.
	 */
	private void createGrid() {
		grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(20);
		grid.setVgap(20);

		createControls();

		grid.add(firstType, 0, 0);
		grid.add(typeXOptions, 0, 1);
		grid.add(secondType, 0, 3);
		grid.add(typeYOptions, 0, 4);
		grid.add(done, 0, 6);

		GridPane.setHalignment(firstType, HPos.LEFT);
		GridPane.setValignment(firstType, VPos.CENTER);

		GridPane.setHalignment(typeXOptions, HPos.LEFT);
		GridPane.setValignment(typeXOptions, VPos.CENTER);

		GridPane.setHalignment(secondType, HPos.LEFT);
		GridPane.setValignment(secondType, VPos.CENTER);

		GridPane.setHalignment(typeYOptions, HPos.LEFT);
		GridPane.setValignment(typeYOptions, VPos.CENTER);

		GridPane.setHalignment(done, HPos.CENTER);
		GridPane.setValignment(done, VPos.BOTTOM);
	}

	/**
	 * Creates the elements the user will interact with.
	 */
	private void createControls() {
		firstType = new Label("First type");
		secondType = new Label("Second type");
		typeXOptions = new ChoiceBox<Object>();
		typeYOptions = new ChoiceBox<Object>();
		done = new Button("Create monster");

		typeXOptions.setItems(FXCollections.observableArrayList("Water type", "Fire type", "Air type", "Earth type"));
		typeXOptions.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> ov, Number value, Number new_value) {
				typeX = types[new_value.intValue()];
			}
		});

		typeYOptions.setItems(
				FXCollections.observableArrayList("Water type", "Fire type", "Air type", "Earth type", "No type"));
		typeYOptions.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> ov, Number value, Number new_value) {
				typeY = types[new_value.intValue()];
			}
		});

		done.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				MediaPlayer buttonClick = new MediaPlayer(new Media(getClass().getResource("/resources/button-30.mp3").toExternalForm()));
				buttonClick.play();
				
				createMonster();
			}
		});
	}

	/**
	 * Creates a Monster for the appropriate player and passes on the game to the
	 * next window. Once all players have their respective Monsters, the game will
	 * execute.
	 */
	private void createMonster() {
		if (game.getPlayer1().getPlayerMonster() == null && typeX != null) {
			game.getPlayer1().setPlayerMonster(new Monster(typeX, typeY));

			MonsterCreationWindow creationWindow = new MonsterCreationWindow(game);
			creationWindow.show(new Stage(), game.getPlayer2().getName());
			currentStage.close();
			
		} else if (typeX != null) {
			game.getPlayer2().setPlayerMonster(new Monster(typeX, typeY));
			
			Board gameBoard = new Board(game, game.getPlayer1().getPlayerMonster().getTypeY() == null, typeY == null);
			gameBoard.show(new Stage());
			
			currentStage.close();
		}

	}

	public void show(Stage stage, String playerName) {
		currentStage = stage;
		Scene scene = new Scene(grid, 400, 300);
		scene.getStylesheets().add(getClass().getResource("/resources/MonsterApplication.css").toExternalForm());
		stage.setScene(scene);
		stage.setResizable(false);
		stage.getIcons().add(new Image(getClass().getResource("/resources/Icon-Storn.jpg").toExternalForm()));
		stage.setTitle(playerName + "'s monster");
		stage.centerOnScreen();
		stage.show();
	}
}
